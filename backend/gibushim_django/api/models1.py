from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator
from datetime import date
from django.conf import settings
import os
from django.utils.timezone import now 
from api import logging_conf

MEDIA_ROOT = settings.MEDIA_ROOT
IS_NEW_IMMIGRENT_VALUE_ERROR = "התקבל ערך לא חוקי בעמודת עולה חדש בשורה {}"
DISQUALIFYING_MEDICAL_TRAIT_VALUE_ERROR = "התקבל ערך לא חוקי בעמודת סעיף רפואי פוסל בשורה {}"
KABHA_VALUE_ERROR = "התקבל ערך לא חוקי בעמודת קבא בשורה {}(יש לשים לב שבעמודה זו חייב להיות מספר)"
TZADACH_VALUE_ERROR = "התקבל ערך לא חוקי בעמודת צדכ בשורה {}(יש לשים לב שבעמודה זו חייב להיות מספר)"
DAPAR_VALUE_ERROR = "התקבל ערך לא חוקי בעמודת דפר בשורה {}(יש לשים לב שבעמודה זו חייב להיות מספר)"
DAPAR_SPECIFIC_VALUES_ERROR = "התקבל ערך לא חוקי בעמודת דפר בשורה {}(יש לשים לב שבעמודה זו המספר חייב להתחלק ב - 10)"
MEDICAL_PROFILE_SPECIFIC_VALUE_ERROR = "התקבל ערך לא חוקי בעמודת דפר בשורה {}(יש לשים לב שבעמודה זו המספר חייב להיות 82 או 97)"
MEDICAL_PROFILE_VALUE_ERROR = "התקבל ערך לא חוקי בעמודת פרופיל בשורה {}(יש לשים לב שבעמודה זו חייב להיות מספר)"


class Gibush(models.Model):
    """
    description: Gibush model
    contains: start date, end date, title and description
    """
    start_date = models.DateField()
    end_date = models.DateField()
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, null=True)


class CsvParseError(ValueError):
    def __init__(self, message, *args, **kwargs):
        self.message = message
        logging_conf.logger.debug("error on upload file - {}".format(self.message))
        super(CsvParseError).__init__(*args, **kwargs)


class ParticipentManager(models.Manager):
    """
    description: manager model for participent, work on participent object.
    get list and add row to participent table.
    """
    def create(self, participent_dict, *args):
        kwargs = participent_dict.copy()
        return super(ParticipentManager, self).create(*args, **kwargs)

    def clean(self, participent_dict, row_number):
        self.participent_dict = participent_dict
        # is_new_immigrant validation
        if not self.check_edit_bool("is_new_immigrant"):
            error = CsvParseError(IS_NEW_IMMIGRENT_VALUE_ERROR.format(row_number))
            raise error

        # disqualifying_medical_trait validation
        if not self.check_edit_bool("disqualifying_medical_trait"):
            error = CsvParseError(DISQUALIFYING_MEDICAL_TRAIT_VALUE_ERROR.format(row_number))
            raise error

        # kabha int validation
        try:
            self.participent_dict["kabha"] = int(participent_dict["kabha"].strip())
        except ValueError:
            error = CsvParseError(KABHA_VALUE_ERROR.format(row_number))
            raise error

        # tzadach int validation
        try:
            self.participent_dict["tzadach"] = int(participent_dict["tzadach"].strip())
        except ValueError:
            error = CsvParseError(TZADACH_VALUE_ERROR.format(row_number))
            raise error

        # dapar int validation
        try:
            self.participent_dict["dapar"] = int(participent_dict["dapar"].strip())
        except ValueError:
            error = CsvParseError(DAPAR_VALUE_ERROR.format(row_number))
            raise error

        # dapar multiply validation
        if not self.check_dapar():
            error = CsvParseError(DAPAR_SPECIFIC_VALUES_ERROR.format(row_number))
            raise error

        # profile int validation
        try:
            self.participent_dict["medical_profile"] = int(participent_dict["medical_profile"].strip())
        except ValueError:
            error = CsvParseError(MEDICAL_PROFILE_VALUE_ERROR.format(row_number))
            raise error

        # profile value validation
        if not self.check_profile():
            error = CsvParseError(MEDICAL_PROFILE_SPECIFIC_VALUE_ERROR.format(row_number))
            raise error

        self.create(self.participent_dict)
        return True
        

    def check_profile(self):
        profile_options = [82,97]
        if self.participent_dict["medical_profile"] in profile_options:
            return True
        else:
            return False

    def check_dapar(self):
        if self.participent_dict["dapar"] % 10 == 0:
            return True
        else:
            return False    

    def check_edit_bool(self, value):
        if self.participent_dict[value].strip() == "כן":
            self.participent_dict[value] = True
            return True
        elif self.participent_dict[value].strip() == "לא":
            self.participent_dict[value] = False
            return True
        else:
            return False


class Participent(models.Model):
    """
    description: participent model(all the types are string because of the csv)
    contains: first_name, last_name,taz,personal_number,birth_date,first_phone,second_phone,
    third_phone,address,is_new_immigrant,enlistment_status,kabha,dapar,tzadach,
    medical_profile,disqualifying_medical_trait
    """
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    identity_number = models.CharField(max_length=9, validators=[MinLengthValidator(9)], null=True)
    personal_number = models.CharField(max_length=8, null=True, validators=[MinLengthValidator(7)])
    birth_date = models.CharField(max_length=100, null=True)
    first_phone = models.CharField(max_length=100, null=True)
    second_phone = models.CharField(max_length=100, null=True, blank=True)
    third_phone = models.CharField(max_length=100, null=True, blank=True)
    address = models.CharField(max_length=100, null=True)
    is_new_immigrant = models.BooleanField(null=True)
    enlistment_status = models.CharField(max_length=100, null=True)
    kabha = models.IntegerField(null=True, validators=[MinValueValidator(40), MaxValueValidator(56)])
    dapar = models.IntegerField(null=True, validators=[MinValueValidator(10), MaxValueValidator(90)])
    tzadach = models.IntegerField(null=True, validators=[MinValueValidator(1), MaxValueValidator(50)])
    medical_profile = models.IntegerField(null=True, validators=[MinValueValidator(82), MaxValueValidator(97)])
    disqualifying_medical_trait = models.BooleanField(null=True)
    source_file =  models.CharField(max_length=1000000000, null=True)
    pic = models.CharField(max_length=1000000000, null=True, blank=True)

    objects = ParticipentManager()


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    today = date.today()
    print((os.path.join(MEDIA_ROOT, str(instance.date_file), filename)) )
    return (os.path.join(MEDIA_ROOT, str(instance.date_file), filename))  


class CsvFile(models.Model):
    """ 
    description: participent model(all the types are string because of the csv)
    contains: first_name, last_name,taz,personal_number,birth_date,first_phone,second_phone,
    third_phone,address,is_new_immigrant,malshabiut_status,kabha,dapar,tzadach,
    profile,disqualifying_medical_trait
    """
    date_file = models.DateField(("Date"), auto_now_add=True, null=True)
    csv_file = models.FileField(upload_to=user_directory_path, max_length=1000000)


class GibushStats(models.Model):
    """ 
    description: GibushStats Model 
    contains: myun_id, soldier_id, first_stage_group, second_stage_group, first_stage_sozio,
    second_stage_sozio, final_grade, final_rating, comment, participent_status
    """

    myun_id = models.IntegerField()
    soldier_id = models.IntegerField()
    first_stage_group = models.IntegerField(null=True)
    second_stage_group = models.IntegerField(null=True)
    first_stage_sozio = models.DecimalField(max_digits=12, decimal_places=9, null=True)
    second_stage_sozio = models.DecimalField(max_digits=12, decimal_places=9, null=True)
    first_stage_grade = models.DecimalField(max_digits=12, decimal_places=9, null=True)
    second_stage_grade = models.DecimalField(max_digits=12, decimal_places=9, null=True)
    final_grade = models.DecimalField(max_digits=12, decimal_places=9, null=True)
    final_rating = models.IntegerField(null=True)
    comment = models.CharField(max_length=1000, null=True)
    participent_status = models.CharField(max_length=1000, default="נקלט")
    passed_gibush = models.BooleanField(default=False)



class GibushGroup(models.Model):
    """
    description: GibushGroup Model, create group per stage
    """
    myun_id = models.IntegerField()
    stage = models.IntegerField()
    group_id = models.IntegerField()

class AssessorManager(models.Manager):
    """
    description: manager model for assessor, work on assessor object.
    get assessor row and create it.
    """
    def create(self, assessor_dict, *args):
        print(assessor_dict)
        kwargs = assessor_dict.copy()
        return super(AssessorManager, self).create(*args, **kwargs)


class Assessor(models.Model):
    """
    description: assessors model, build the assessors
    contains: first_name, last_name, identity_number, unit.
    """
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    identity_number = models.CharField(max_length=9, validators=[MinLengthValidator(9)], null=True)
    unit = models.CharField(max_length=9, default='767', null=True)

    objects = AssessorManager()


class AssessorInGroup(models.Model):
    """
    description: AssessorsInGroups model, connect group and assessors tables
    contains: group_id, assessors_id, role.
    """
    myun_id = models.IntegerField()
    stage = models.IntegerField(null=True)
    group_id = models.IntegerField(null=True)
    assessor_id = models.IntegerField(null=True)
    role = models.CharField(max_length=100, null=True)


class AssessorEvaluation(models.Model):
    """
    description: AssessorsEvaluation model, connect grade to soldier and assessor in specific myun 
    contains: myun_id, soldier_id, assessors_id, grade, comment.
    """
    myun_id = models.IntegerField()
    soldier_id = models.IntegerField()
    assessor_id = models.IntegerField()
    stage = models.IntegerField(null=True)
    grade = models.IntegerField(null=True)
    comment = models.CharField(max_length=1000, null=True) 
    

class SozioGibushGrades(models.Model):
    """
    Description: SozioGibushGrades model, connect filler participant and subject participant with their grade in stage
    """
    filler_participent_id = models.IntegerField()
    subject_participent_id = models.IntegerField()
    stage = models.IntegerField()
    grade = models.IntegerField()


class retirements(models.Model):
    """
    Description: Retirements model, retire participant in myun and add data about retirement(reason, days in gibush, stage and date)
    """
    myun_id = models.IntegerField()
    soldier_id = models.IntegerField()
    retirements_reason = models.CharField(max_length=1000)
    days_in_gibush = models.IntegerField(null=True)
    retirement_date = models.DateField(auto_now_add=True)
    retirement_stage = models.IntegerField(null=True)