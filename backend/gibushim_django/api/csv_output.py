import csv


HEADERS = {'first_name':'שם פרטי',
                      'last_name':'שם משפחה',
                      'identity_number':'תז',   
                      'personal_number':'מספר אישי',
                      'birth_date':'תאריך לידה',
                      'first_phone':'טלפון 1',
                      'second_phone':'טלפון 2',
                      'third_phone':'טלפון 3',
                      'address':'כתובת',
                      'is_new_immigrant':'עולה חדש',   
                      'enlistment_status':'סטטוס מלשביות',
                      'kabha':'קבא',
                      'dapar':'דפר',
                      'tzadach':'צדכ',
                      'medical_profile':'פרופיל',
                      'disqualifying_medical_trait':'סעיף רפואי פוסל',
                      'first_stage_group': 'קבוצה שלב א',
                      'first_stage_assessor1' : 'מגבש 1 - שלב א',
                      'first_stage_assessor1_grade': 'מגבש 1 ציון שלב א',
                      'first_stage_assessor1_comment': 'מגבש 1 שלב א מלל',
                      'first_stage_assessor2' : 'מגבש 2 - שלב א',
                      'first_stage_assessor2_grade': 'מגבש 2 ציון שלב א',
                      'first_stage_assessor2_comment': 'מגבש 2 שלב א מלל',
                      'first_stage_sozio': 'סוציומטרי שלב א',
                      'first_stage_grade': 'ציון שלב א',
                      'second_stage_group': 'קבוצה שלב ב',
                      'second_stage_assessor1' : 'מגבש 1 - שלב ב',
                      'second_stage_assessor1_grade': 'מגבש 1 ציון שלב ב',
                      'second_stage_assessor1_comment': 'מגבש 1 שלב ב מלל',
                      'second_stage_assessor2' : 'מגבש 2 - שלב ב',
                      'second_stage_assessor2_grade': 'מגבש 2 ציון שלב ב',
                      'second_stage_assessor2_comment': 'מגבש 2 שלב ב מלל',
                      'second_stage_sozio': 'סוציומטרי שלב ב',
                      'second_stage_grade': 'ציון שלב ב',
                      'final_grade': 'ציון סופי',
                      'participent_status': 'סטטוס בגיבוש'}

GROUPS_HEADERS = {'first_name':'שם פרטי',
                      'last_name':'שם משפחה',
                      'identity_number':'תז',
                      'group_id': 'מספר קבוצה'}


def groups_csv(list_of_dict, response):
    clean_list = clean_groups(list_of_dict)
    keys = clean_list[0].keys()
    dict_writer = csv.DictWriter(response, keys)
    dict_writer.writeheader()
    #for dic in clean_list:
    #    print(dic)
    dict_writer.writerows(clean_list)
    return response


def clean_groups(list_of_dict):
    
    list_groups = []
    x = {}
    for dics in list_of_dict:
        x = {}
        for dic in dics:
            for key, value in GROUPS_HEADERS.items():
                x[value] = dic.get(key, 'Empy')

            list_groups.append(x)
            x = {}
    return list_groups
            

def final_grades_csv(list_of_dict, response):
    list_copy = clean_dict(list_of_dict)
    keys = list_copy[0].keys()
    dict_writer = csv.DictWriter(response, keys)
    dict_writer.writeheader()
    dict_writer.writerows(list_copy)
    return response


def clean_dict(list_of_dict):
    list_copy = []
    for dic in list_of_dict:
        dict_copy = {}
        row_dict = {}
        counter_first_stage = 0
        counter_second_stage = 0
        dict_copy = dic.copy()
        participent_properties = dict_copy.pop("participent")
        assessors_grade_names = dict_copy.pop("assessors_grade_names")
        for assessor_grade in assessors_grade_names:
            assessors = {}
            if assessor_grade["stage"] == 1:
                counter_first_stage += 1
                assessors["first_stage_assessor{}".format(counter_first_stage)] = assessor_grade["full_name"]
                assessors["first_stage_assessor{}_grade".format(counter_first_stage)] = assessor_grade["grade"]
                assessors["first_stage_assessor{}_comment".format(counter_first_stage)] = assessor_grade["comment"]
            elif assessor_grade["stage"] == 2:
                counter_second_stage += 1
                assessors["second_stage_assessor{}".format(counter_first_stage)] = assessor_grade["full_name"]
                assessors["second_stage_assessor{}_grade".format(counter_first_stage)] = assessor_grade["grade"]
                assessors["second_stage_assessor{}_comment".format(counter_first_stage)] = assessor_grade["comment"]
            dict_copy.update(assessors)
            print(assessors)
        dict_copy.update(participent_properties)
        
        for key, value in HEADERS.items():
            try:
                row_dict[HEADERS[key]] = dict_copy[key]
                
            except Exception as e:
                print(e)
                row_dict[value] = ""
        list_copy.append(row_dict)
    return list_copy