import json
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics, status
from django.db.models import F, Q, FilteredRelation,  BooleanField, Case, When, Value, IntegerField
from django.conf import settings
from django.apps import apps
from api.serializers import UserSerializer, ParticipentSerializer, GroupSerializer, GibushSerializer, CsvFileSerializer, FileParseSerializer, GibushStatsSerializer, CreateGroupSerializer, GetGroupSerializer, AssessorSerializer, AssessorInGroupSerializer, AssessorEvaluationSerializer, GroupsAssessorsSerializer, SozioGibushGradesSerializer, ParticipentProfileSerializer, AssessorNameAndGradesSerializer, MarkArrivedParticipentSerializer, ListRetirementsSerializer, CreateRetirementsSerializer
from api.models import Gibush, Participent, CsvFile, CsvParseError, GibushStats, GibushGroup, Assessor, AssessorInGroup, AssessorEvaluation, SozioGibushGrades, retirements
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser,JSONParser, BaseParser
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
import csv
import os
import datetime
from django.conf import settings
from collections import defaultdict 
from io import BytesIO
from xhtml2pdf import pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from zipfile import ZipFile
from django.db import connection
from api.assets import logging_conf, csv_output
import sys
import shutil

MEDIA_ROOT = settings.MEDIA_ROOT

TABLE_FILE_HEADERS = {'שם פרטי': 'first_name',
                      'שם משפחה': 'last_name',
                      'תז': 'identity_number',  
                      'מספר אישי': 'personal_number',
                      'תאריך לידה': 'birth_date',
                      'טלפון 1': 'first_phone',
                      'טלפון 2': 'second_phone',
                      'טלפון 3': 'third_phone',
                      'כתובת': 'address',
                      'עולה חדש': 'is_new_immigrant',  
                      'סטטוס מלשביות': 'enlistment_status',
                      'קבא': 'kabha',
                      'דפר': 'dapar',
                      'צדכ': 'tzadach',
                      'פרופיל': 'medical_profile',
                      'סעיף רפואי פוסל': 'disqualifying_medical_trait'}


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class GibushViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows gibush to be viewed or edited.
    """
    queryset = Gibush.objects.all()
    serializer_class = GibushSerializer
    logging_conf.logger.debug("return all gibushim")


class AssessorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows assessor to be viewed or edited.
    schema: assessor is alredy existing
            when - identity number in assessors
            then - update assessor object

    schema: create new assessor
            when - identity number is unique
            then - create assessor object
    """
    # get lines, for showing all assessors
    queryset = Assessor.objects.all()
    serializer_class = AssessorSerializer
    logging_conf.logger.debug("return all assessor")
    
    def create(self, request, *args, **kwargs):
        # create/update function in assessor
        assessor_by_identity = Assessor.objects.filter(identity_number=request.data["identity_number"])
        if "identity_number" in list(request.data.keys()) and assessor_by_identity.exists():
            file_serializer = AssessorSerializer(assessor_by_identity, data=request.data, partial=True)
        else:
            file_serializer = AssessorSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            logging_conf.logger.debug("assessor {} {} successfully created".format(request.data.get("first_name"), request.data.get("last_name")))
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ParticipentPropertiesViewSet(viewsets.ModelViewSet):
    queryset = Participent.objects.all()
    serializer_class = ParticipentSerializer


class ParticipentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participant to be viewed or edited.
    get = list
    post = create
    scenario: update participant
            when - identity number is existing in paricipant table
            And - update=True in request.data
            then - update participant object

    scenario: update participant pic
            when - identity number in participant
            And - pic in request.data
            then - update participant pic

    scenario: create new participant in myun
            when - identity number is unique
            then - create participant object
            when - myun_id in request.data
            then - create gibushstats object of current participant and myun
    
    scenario: create new participant outside myun
            when - identity number is unique and myun_id not in request.data
            then - create participant object

    scenario: create new participant with existing identity_number
            when - identity number is existing in paricipant table
            And - update=False in request.data
            then - return response 400-bad request, לא ניתן ליצור מועמד עם תעודת זהות קימת 
    
    """
    queryset = Participent.objects.all()
    serializer_class = MarkArrivedParticipentSerializer
    

    def list(self, request, *args, **kwargs):
        """
        Return all assessors with arrived field - sort by arrived and then not arrived
        """
        myun_soldiers = GibushStats.objects.filter(myun_id=request.GET["myun_id"], participent_status="נקלט").values_list("soldier_id", flat=True)
        arrived_participents_copy = Participent.objects.filter(id__in=myun_soldiers).values('id', 'first_name', 'last_name', 'identity_number', 'first_phone', 'address')
        not_arrived_participents_copy = self.queryset.exclude(id__in=myun_soldiers).values('id', 'first_name', 'last_name', 'identity_number', 'first_phone', 'address')
        for participent in arrived_participents_copy:
            participent["arrived"] = True
        for participent in not_arrived_participents_copy:
            participent["arrived"] = False
        participents = list(arrived_participents_copy) + list(not_arrived_participents_copy)
        file_serializer = MarkArrivedParticipentSerializer(participents, many=True)
        logging_conf.logger.debug("return all particpants - arrived/not_arrived")
        gibush_soldiers = GibushStats.objects.all().values_list("soldier_id", flat=True)
        y = Participent.objects.filter(id__in=gibush_soldiers).exclude(id__in=gibush_soldiers, pic=None).values_list("pic", "first_name", "last_name", "id")
        return Response(file_serializer.data)

    def create(self, request, *args, **kwargs):
        soldier_id = request.data.get("soldier_id")
        participent_by_identity = Participent.objects.filter(identity_number=request.data["identity_number"])
        update = False
        if "identity_number" in list(request.data.keys()) and participent_by_identity.exists():
            if request.data.get("update"):
                file_serializer = ParticipentSerializer(participent_by_identity, data=request.data, partial=True)
                update = True
                soldier_id = participent_by_identity.values_list("id", flat=True)[0]
            else:
                return Response("לא ניתן ליצור מועמד עם תעודת זהות קיימת", status=status.HTTP_400_BAD_REQUEST)
        else:
            file_serializer = ParticipentSerializer(data=request.data)
        if 'myun_id' in list(request.data.keys()):
            myun_id = int(request.data['myun_id'])
            myun = Gibush.objects.filter(id=myun_id)
        else:
            myun = None

        if file_serializer.is_valid():
            file_serializer.save()
            if myun:
                if not soldier_id:
                    soldier_id = Participent.objects.filter(identity_number=request.data["identity_number"]).values_list('id', flat=True)[0]
                gibush_serializer = GibushStatsSerializer(data={"myun_id": myun_id, "soldier_id": int(soldier_id)})
                if gibush_serializer.is_valid():
                    gibush_serializer.save()
                    if not update:
                        logging_conf.logger.debug("participant {} {} and stats has successfully created".format(request.data.get("first_name"), request.data.get("last_name")))
                    else:
                        logging_conf.logger.debug("participant {} {} and stats has successfully updated".format(request.data.get("first_name"), request.data.get("last_name")))
                else:
                    logging_conf.logger.debug("participant {} {} has successfully updated".format(request.data.get("first_name"), request.data.get("last_name")))
            else:
                if request.data.get("pic"):
                    logging_conf.logger.debug("participant {} pic changed successfully".format(request.data.get("identity_number")))
                else:
                    logging_conf.logger.debug("participant {} creted outside the gibush".format(request.data.get("identity_number")))
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            logging_conf.logger.debug("{} {} has not successfuly created, {}".format(request.data.get("first_name"), request.data.get("last_name"), file_serializer.errors))
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FileUploadView(APIView):
    """
    Title : API endpoint that allows post of pk(gibush) and file name
    Description : using file upload parser to get file.
                  call to fileparse class and call the post function for parsing and analyzing data.
    """
    parser_class = (FileUploadParser,)
    def post(self, request, *args, **kwargs):
        file_serializer = CsvFileSerializer(data=request.data) 
        if file_serializer.is_valid():
            file_serializer.save()
            x = FileParse()
            x.post()
            logging_conf.logger.debug("file uploaded successfully")
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            logging_conf.logger.debug("file doesnt uploaded, error - {}".format(file_serializer.errors))
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FileParse():
    """
    Description: parse csv file, get file name and primarykey(to know the file location)
    it reads the file(create dict of header and value).
    and create dict of the table header the content from the file. 
    call the clean func for checking and creating objects from csv.
    """
    def __init__(self):
        super(FileParse).__init__()

    def parse_csv(self, csv_filename, csv_pk): 
        with open(os.path.join(MEDIA_ROOT, str(csv_filename)), encoding="ISO-8859-8") as csvfile:
            reader = csv.DictReader(csvfile)
            row_number = 1
            for row in reader:
                row_dict = {}
                row_number += 1
                for file_key in row.keys():
                    table_file_key = file_key.replace(u'\xa0', " ").strip()
                    row_dict[TABLE_FILE_HEADERS[table_file_key]] = row[file_key]
                row_dict['source_file'] = os.path.join(MEDIA_ROOT, str(csv_filename))
                Participent.objects.clean(row_dict, row_number) 

    def post(self, *args, **kwargs):
        try:
            csv_obj = CsvFile.objects.order_by('-id')[0]
            self.parse_csv(csv_obj.csv_file, csv_obj.date_file)
            return Response("success", status=status.HTTP_201_CREATED)
        except CsvParseError as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)


class AssessorInGroupView(APIView):
    """
    API endpoint that allows get and post data of groups and assessors
    get all assessors from specific group
    scenario: update assessor association into group
            when - assessor is alredy in myun and stage(there is object of assessor, myun, stage in AssessorInGroup table)
            and - group_id in request.data
            then - update group field in assessorInGroup object per myun and stage 

    scenario: unassociate assessor to group(delete assessor association into group)
            when - assessor is alredy in myun and stage(there is object of assessor, myun, stage in AssessorInGroup table)
            and - group_id not in request.data
            then - delete AssessorInGroup object of specific group, myun, assessor

    scenario: create assessor assosiation into group
            when - get assessor_id, myun_id, stage
            and - there isn't assessor object for this specific assessor in myun and stage
            then - create assessorInGroup object
    """
    def get(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value> 
        example: field=group_id, group_id=3
        """
        assessors_in_group = GibushGroup.objects.filter(group_id=request.GET.get("group_id"), stage=request.GET.get("stage"), myun_id=request.GET.get("myun_id")).values("group_id", "stage", "myun_id")
        assessors_in_group = AssessorInGroup.objects.filter(group_id=assessors_in_group[0]["group_id"], stage=assessors_in_group[0]["stage"], myun_id=assessors_in_group[0]["myun_id"]).values_list("assessor_id", flat=True)
        queryset = Assessor.objects.filter(id__in=assessors_in_group)
        serializer_class = AssessorSerializer(queryset, many=True)
        logging_conf.logger.debug("return all assessors from specific group")
        return Response(serializer_class.data)

    def post(self, request, *args, **kwargs):
        assessors = AssessorInGroup.objects.filter(assessor_id=request.data["assessor_id"], myun_id=request.data["myun_id"], stage=request.data["stage"])
        if assessors.exists():
            if not request.data.get("group_id"):
                cursor = connection.cursor()
                cursor.execute("DELETE FROM api_assessoringroup WHERE (assessor_id, myun_id, stage) = (%s,%s,%s)", [request.data["assessor_id"], request.data["myun_id"], request.data["stage"]])
                logging_conf.logger.debug("assessor num {}, has unassociated to group".format(request.data.get("assessor_id")))
                return Response("success", status=status.HTTP_201_CREATED)
            else:
                file_serializer = AssessorInGroupSerializer(assessors, data=request.data, partial=True)
                logging_conf.logger.debug("assessor num {}, has associated to group {}".format(request.data.get("assessor_id"), request.data.get("group_id")))
        else:
            file_serializer = AssessorInGroupSerializer(data=request.data)
            logging_conf.logger.debug("assessor num {}, has unassociated to group".format(request.data.get("assessor_id")))
        if file_serializer.is_valid():
            file_serializer.save()
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AssessorEvaluationView(viewsets.ModelViewSet):
    """
    API endpoint that allows get and post of assessors, soldiers, grades and comments
    get = list
    get all assessors per group in stage
    post = create
    scenario : create assessor evaluation
            when - get assessor, myun_id, stage, soldier
            and - the object isn't exist
            then - create new object with those fields + (comment, grade)
            and - calculate stage and final grades for soldier

    scenario : update  assessor evaluation
            when - get assessor, myun_id, stage, soldier
            and - the object exists
            then - update object with those fields + (comment, grade)
            and - calculate stage, sozio and final grades for soldier

    """
    queryset = AssessorEvaluation.objects.all()
    serializer_class = AssessorEvaluationSerializer

    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        request_dict = {}
        if request.GET.get("first_stage_group"):
            group_participants = GibushStats.objects.filter(myun_id=request.GET.get("myun_id"), first_stage_group=request.GET.get("first_stage_group")).values_list("soldier_id", flat=True)
            stage = 1
        elif request.GET.get("second_stage_group"):
            group_participants = GibushStats.objects.filter(myun_id=request.GET.get("myun_id"), second_stage_group=request.GET.get("second_stage_group")).values_list("soldier_id", flat=True)
            stage = 2
        queryset = AssessorEvaluation.objects.filter(soldier_id__in=group_participants, stage=stage)
        file_serializer = AssessorEvaluationSerializer(queryset, many=True)
        return Response(file_serializer.data)

    def create(self, request, *args, **kwargs):
        evaluation = AssessorEvaluation.objects.filter(soldier_id=request.data["soldier_id"], myun_id=request.data["myun_id"], assessor_id=request.data["assessor_id"], stage=request.data["stage"])
        if evaluation.exists():
            logging_conf.logger.debug("assessor num {}, has updated grade for soldier num {}".format(request.data.get("assessor_id"), request.data.get("soldier_id")))
            file_serializer = AssessorEvaluationSerializer(evaluation, data=request.data, partial=True)
        else:
            logging_conf.logger.debug("assessor num {}, has evaluated soldier num {}".format(request.data.get("assessor_id"), request.data.get("soldier_id")))
            file_serializer = AssessorEvaluationSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            ans = calculate_grades(request.data)
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def calculate_grades(myun_soldier):
    """
    calculate specific stage grades, sozio and final grade 
    """
    full_data = myun_soldier.copy()
    del myun_soldier['stage']
    # for calculate final grade 
    total_grades = AssessorEvaluation.objects.filter(myun_id=int(myun_soldier["myun_id"]), soldier_id=int(myun_soldier["soldier_id"])).values_list("grade", flat=True)
    if total_grades.exists():
        final_grade = ((sum(total_grades) / len(total_grades)) / 6) * 100
        grades_per_stage = AssessorEvaluation.objects.filter(myun_id=int(full_data["myun_id"]), soldier_id=int(full_data["soldier_id"]), stage=int(full_data["stage"])).values_list("grade", flat=True)
        final_grade_stage = sum(grades_per_stage) / len(grades_per_stage)
    else:
        final_grade = 0
        final_grade_stage = 0
    # for calculate specific stage 
    final_grade_stage = (final_grade_stage / 6) * 100
    participant_stat = GibushStats.objects.filter(soldier_id=int(myun_soldier["soldier_id"]), myun_id=int(myun_soldier["myun_id"]))
    try:
        sozio_grades = participant_stat.values("first_stage_sozio", "second_stage_sozio")[0]
    except IndexError as e:
        sozio_grades = {}

    first_stage_sozio = sozio_grades.get("first_stage_sozio")
    second_stage_sozio = sozio_grades.get("second_stage_sozio")
    if first_stage_sozio:
        first_stage_sozio = int(first_stage_sozio)
    else:
        first_stage_sozio = 0

    if second_stage_sozio:
        second_stage_sozio = int(second_stage_sozio)
    else:
        second_stage_sozio = 0
    final_sozio = sum([first_stage_sozio, second_stage_sozio]) / 2
    final_grade = 0.5 * final_grade + 0.5 * final_sozio
    if full_data["stage"] == 1:
        logging_conf.logger.debug("soldier num {}, has updated stage {} grade and final grade".format(myun_soldier.get("soldier_id"), full_data.get("stage")))
        participant_stat.update(first_stage_grade=final_grade_stage, final_grade=final_grade)
    elif full_data["stage"] == 2:
        logging_conf.logger.debug("soldier num {}, has updated stage {} grade and final grade".format(myun_soldier.get("soldier_id"), full_data.get("stage")))
        participant_stat.update(second_stage_grade=final_grade_stage, final_grade=final_grade)


class EnterGibushStatsView(viewsets.ModelViewSet):
    """
    Description: API endpoint that allows post stats of participant in specific gibush
    post = create
    create/update GibushStats object, depend if the object with myun_id, soldier_id exists
    """
    queryset = GibushStats.objects.all()
    serializer_class = GibushStatsSerializer
    
    def create(self, request, *args, **kwargs):
        #TODO add filter by myun_id
        participant_stat = GibushStats.objects.filter(soldier_id=request.data["soldier_id"], myun_id=request.data["myun_id"])
        if "soldier_id" in list(request.data.keys()) and participant_stat.exists():
            logging_conf.logger.debug("soldier num {}, has updated stats".format(request.data.get("soldier_id")))
            file_serializer = GibushStatsSerializer(participant_stat, data=request.data, partial=True)
        else:
            logging_conf.logger.debug("soldier num {}, has accepted to gibush".format(request.data.get("soldier_id")))
            file_serializer = GibushStatsSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GibushGroupsView(APIView):
    """
    Description: API endpoint that allows create groups, split into groups and list all the groups and their assessors
    get: list all the groups and their assessor
        groups with assessors assign - 
            return json of AssessorsInGroups - dict: assessor_id,
            group_id, stage, role, first name, last name, people in group.
        groups without assessors assign - 
            return json of GibushGroup - dict: group_id, stage, people in group
            first name="", last name="", assessor_id="", identity number="", role="", unit=""
    
    post: create groups and  split participants into groups.
        scenario: create groups and split into groups
                when - get number of groups to create in specific gibush, stage
                and - there is no groups in this stage in gibush
                then - call create groups(1 - num_of_groups)
                and  - split particippants into groups(by new_ole and address)
        
        scenario: update groups and split into groups
                when - get number of groups to create in specific gibush, stage
                and - there is groups alredy in this stage in gibush
                then - delete all the existing groups
                when - there is no groups in this stage in gibush
                then - call create groups(1 - num_of_groups)
                and  - split particippants into groups(by new_ole and address)
    """
    
    def get(self, request, *args, **kwargs):
        request_dict = {}
        for key, value in request.GET.items():
            request_dict[key] = int(value[0])
        assessors_per_group = AssessorInGroup.objects.filter(**request_dict).values("assessor_id", "group_id", "stage", "role")
        if assessors_per_group.exists():
            
            for dic in assessors_per_group:
                if "stage" in request_dict:
                    if request_dict['stage'] == 1:
                        if dic["group_id"]:
                            dic["people_in_group"] = GibushStats.objects.filter(myun_id=request_dict["myun_id"], first_stage_group=int(dic["group_id"]), participent_status="נקלט").count()
                        else:
                            list(assessors_per_group).remove(dic)
                    elif request_dict['stage'] == 2: 
                        if dic["group_id"]:
                            dic["people_in_group"] = GibushStats.objects.filter(myun_id=request_dict["myun_id"], second_stage_group=int(dic["group_id"]), participent_status="נקלט").count()
                        else:
                            list(assessors_per_group).remove(dic)
                asssessor_name = Assessor.objects.filter(id=dic["assessor_id"]).values("first_name", "last_name", "identity_number", "unit")
                dic["first_name"] = asssessor_name[0]["first_name"]
                dic["last_name"] = asssessor_name[0]["last_name"]
                dic["identity_number"] = asssessor_name[0]["identity_number"]
                dic["unit"] = asssessor_name[0]["unit"]
            try:
                groups_without_assessors = GibushGroup.objects.filter(**request_dict).exclude(group_id__in=assessors_per_group.values_list("group_id", flat=True), stage=request_dict['stage']).values("group_id", "stage")
            except:
                groups_without_assessors = GibushGroup.objects.filter(**request_dict).exclude(group_id__in=assessors_per_group.values_list("group_id", flat=True)).values("group_id", "stage")
            if groups_without_assessors:
                for dic in groups_without_assessors:
                    if "stage" in request_dict:
                        if request_dict['stage'] == 1:
                            dic["people_in_group"] = GibushStats.objects.filter(myun_id=request_dict["myun_id"],first_stage_group=int(dic["group_id"])).count()
                        elif request_dict['stage'] == 2: 
                            dic["people_in_group"] = GibushStats.objects.filter(myun_id=request_dict["myun_id"],second_stage_group=int(dic["group_id"])).count()
                    dic["first_name"] = ""
                    dic["last_name"] = ""
                    dic["assessor_id"] = ""
                    dic["identity_number"] = ""
                    dic["unit"] = ""
                    dic["role"] = ""
                groups = list(assessors_per_group) + list(groups_without_assessors)
            else:
                groups = assessors_per_group
            file_serializer = GroupsAssessorsSerializer(groups, many=True)
        else:
            queryset = GibushGroup.objects.filter(**request_dict)
            file_serializer = GetGroupSerializer(queryset, many=True)
        if request_dict.get("stage"):
            logging_conf.logger.debug("return all groups in stage {}, num of participants and assessors".format(request_dict.get("stage")))
        else:
            logging_conf.logger.debug("return all assessors data and their groups")
        return Response(file_serializer.data)

    def post(self, request, *args, **kwargs):
        file_serializer = CreateGroupSerializer(data=request.data) 
        if file_serializer.is_valid():
            self.group_ids = GibushGroup.objects.filter(myun_id=request.data["myun_id"], stage=request.data["stage"]).values_list("group_id", flat=True)
            cursor = None
            if self.group_ids.exists():
                cursor = connection.cursor()
                cursor.execute("DELETE FROM api_gibushgroup WHERE (myun_id,stage) = (%s,%s)", [request.data["myun_id"], request.data["stage"]])
                logging_conf.logger.debug("all groups in stage {} of gibush {} has deleted".format(request.data.get("myun_id"), request.data.get("stage")))
            self.biggest_id = 1
            groups = []
            group_properties = {"stage" : request.data["stage"], "myun_id": request.data["myun_id"]}
            for i in range(self.biggest_id, self.biggest_id + int(request.data["num_of_groups"])):
                if cursor:
                    x = AssessorInGroup.objects.all().values_list('group_id')
                group_properties["group_id"] = i
                groups.append(GibushGroup(**group_properties))
            GibushGroup.objects.bulk_create(groups)
            logging_conf.logger.debug("groups in stage {} of gibush {} has created".format(request.data.get("myun_id"), request.data.get("stage")))
            self.data = request.data
            self.split_into_groups()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def split_into_groups(self, *args, **kwargs):
        """
        get json of {myun_id:<int>, num_of_groups:<int>, stage:1,2}
        then split to groups
        """
        stage_attribute = {'1': "first_stage_group", '2': "second_stage_group"}
        stage_group = {}
        group_index = 1
        group_immigrants_dict = {}
        dict_myun_address_id = {}
        # all myun soldiers
        myun_soldiers = GibushStats.objects.filter(myun_id=self.data["myun_id"], participent_status="נקלט").values_list("soldier_id", flat=True)
        if self.data["stage"] == str(2):
            myun_soldiers = myun_soldiers.order_by("first_stage_groups")
        # all soldiers (id,adrress)
        myun_address_id = Participent.objects.filter(id__in=myun_soldiers).values_list("address", "id").order_by("-id")
        # all soldiers {address: id}
        {dict_myun_address_id.setdefault(address, []).append(soldier_id) for address, soldier_id in myun_address_id}            
        num_of_groups = int(self.data["num_of_groups"])
        # create group_immigrant dict with 0 for counter
        for i in range(self.biggest_id, self.biggest_id + int(self.data["num_of_groups"])):
            group_immigrants_dict[i] = 0
        # run on soldiers address and ids in address
        for city, soldier_ids in dict_myun_address_id.items():
            # run on ids
            for i in range(len(soldier_ids)):
                current_group = (group_index % num_of_groups) + 1
                stage_group[stage_attribute[self.data["stage"]]] = current_group
                if Participent.objects.filter(id=soldier_ids[i], is_new_immigrant=True):
                    immigrants_per_group = list(group_immigrants_dict.values())
                    min_immigrant_in_group = min(immigrants_per_group)
                    if group_immigrants_dict.get(current_group) == min_immigrant_in_group:
                        GibushStats.objects.filter(soldier_id=soldier_ids[i]).update(**stage_group)
                        group_immigrants_dict[current_group] += 1
                    else:
                        while group_immigrants_dict[current_group] != min_immigrant_in_group:
                            if current_group == num_of_groups:
                                current_group = 1
                            else:
                                current_group += 1

                        stage_group[stage_attribute[self.data["stage"]]] = current_group
                        GibushStats.objects.filter(soldier_id=soldier_ids[i]).update(**stage_group)
                        group_immigrants_dict[current_group] += 1
                else:
                    GibushStats.objects.filter(soldier_id=soldier_ids[i]).update(**stage_group)
                group_index += 1
        logging_conf.logger.debug("all participnts in stage {} of gibush {} has splited into groups".format(request.data.get("myun_id"), request.data.get("stage")))
        return "t"



class ReturnParticipentList(APIView):
    """
    Description: API endpoint that allows filter data from participent table and gibush stats table - can handle with combinations of headers and values
    headers: everything
    """
    def get(self, request, *args, **kwargs):
        participent_fields = Participent._meta.fields
        gibush_stats_fields = GibushStats._meta.fields
        participent_fields_list = [participent.name for participent in participent_fields]
        gibush_stats_fields_list = [stat.name for stat in gibush_stats_fields]
        request_dict_participent = {}
        request_dict_gibushstats = {}
        myun_soldiers = GibushStats.objects.filter(participent_status="נקלט")
        for key, value in request.GET.items():
            if key in participent_fields_list: 
                request_dict_participent[key] = value
            elif key in gibush_stats_fields_list and key != "id":
                request_dict_gibushstats[key] = value
            else:
                if key == "stage":
                    if value == '1':
                        myun_soldiers = GibushStats.objects.filter(first_stage_group=None).values_list("soldier_id", flat=True)
                        myun_soldiers = GibushStats.objects.all().exclude(soldier_id__in=myun_soldiers).filter(participent_status="נקלט")
                    else:
                        myun_soldiers = GibushStats.objects.filter(second_stage_group=None).values_list("soldier_id", flat=True)
                        myun_soldiers = GibushStats.objects.all().exclude(soldier_id__in=myun_soldiers).filter(participent_status="נקלט")
        myun_soldiers = myun_soldiers.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        queryset = Participent.objects.filter(id__in=myun_soldiers, **request_dict_participent).order_by('last_name')
        logging_conf.logger.debug("return all participants by filters: {}, {} ".format(list(request_dict_gibushstats.keys()), list(request_dict_participent.keys())))
        file_serializer = ParticipentSerializer(queryset, many=True)
        return Response(file_serializer.data)


class SozioGibushGradesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows sozio grades to be viewed or edited.
    get = list
        return all sozio grades of group per stage
    post = create
        scenario : participant has evaluated by all the group
                when - get list of filler_participent_id, subject_participant_id, grade
                then - create objects per filler_participant_id
                when - participant has evaluated by all the other participants in group(number people in group - 1)
                then - calculate final sozio grade of stage

        scenario : participant evaluate all group(and doesn't evaluated by all the group)
                when - get list of filler_participent_id, subject_participant_id, grade
                then - create objects per filler_participant_id
                when - participant hasn't evaluated by all the other participants in group(number people in group - 1)
                then - pass
    """
    queryset = SozioGibushGrades.objects.all()
    serializer_class = SozioGibushGradesSerializer
    def list(self, request, *args, **kwargs):
        """
        return all sozio grades of group per stage
        """
        request_dict = {}
        if request.GET.get("first_stage_group"):
            group_participants = GibushStats.objects.filter(first_stage_group=request.GET.get("first_stage_group")).values_list("soldier_id", flat=True)
            stage = 1
        elif request.GET.get("second_stage_group"):
            group_participants = GibushStats.objects.filter(second_stage_group=request.GET.get("second_stage_group")).values_list("soldier_id", flat=True)
            stage = 2
        queryset = SozioGibushGrades.objects.filter(filler_participent_id__in=group_participants, stage=stage)
        file_serializer = SozioGibushGradesSerializer(queryset, many=True)
        logging_conf.logger.debug("return all sozio grades by participant num {}".format(request_dict.get("filler_participant_id")))
        return Response(file_serializer.data)

    def create(self, request, *args, **kwargs):
        keys_for_update = ["filler_participent_id", "subject_participent_id", "stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioGibushGrades.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioGibushGradesSerializer(grades, data=sozio_dict, partial=True)
            else:
                file_serializer = SozioGibushGradesSerializer(data=sozio_dict)

            if file_serializer.is_valid():
                file_serializer.save()                
                logging_conf.logger.debug("sozio grades has accepted successfully for paticipant number {}".format(sozio_dict.get("subject_participant_id")))
                self.subject_id = sozio_dict["subject_participent_id"]
                self.stage = sozio_dict["stage"]
                self.by_subject = SozioGibushGrades.objects.filter(subject_participent_id=self.subject_id, stage=self.stage)
                self.was_subject = self.by_subject.count()
                self.gibushstats = GibushStats.objects.filter(soldier_id=self.subject_id)
                if self.stage == 1:
                    self.group = self.gibushstats.values("first_stage_group")
                    self.people_in_group = GibushStats.objects.filter(participent_status="נקלט", first_stage_group=self.group[0]["first_stage_group"]).count() - 1
                elif self.stage == 2:
                    self.group = self.gibushstats.values("second_stage_group")
                    self.people_in_group = GibushStats.objects.filter(participent_status="נקלט", second_stage_group=self.group[0]["second_stage_group"]).count() - 1
                
                self.calculate_sozio()
            else:
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)

    # r=100-(100/((p-1)^{2}-p))*(SUM-(p-1))))
    def calculate_sozio(self):
        if self.was_subject == self.people_in_group:
            self.grades = self.by_subject.values_list("grade", flat=True)
            p = self.was_subject
            try:
                result = 100 - ((100 / ((p ** 2) - p)) * (sum(self.grades) - p))
            except ZeroDivisionError as e:
                result = 100
            result = "%.8f" % result
            if self.stage == 1:

                file_serializer = GibushStatsSerializer(self.gibushstats, {"first_stage_sozio": float(result)}, partial=True)
                logging_conf.logger.debug("sozio grades for stage {}, has calculated successfully for paticipant number {}".format(self.stage, self.gibushstats.values_list("soldier_id")[0]))
                logging_conf.logger.debug("{} gibush stats, {} result".format(self.gibushstats, float(result)))
                logging_conf.logger.debug("serializer {}".format(file_serializer))
            else:
                logging_conf.logger.debug("sozio grades for stage {}, has calculated successfully for paticipant number {}".format(self.stage, self.gibushstats.values_list("soldier_id")[0]))
                file_serializer = GibushStatsSerializer(self.gibushstats, {"second_stage_sozio": float(result)}, partial=True)
                logging_conf.logger.debug("{} gibush stats, {} result".format(self.gibushstats, float(result)))
                logging_conf.logger.debug("serializer {}".format(file_serializer))

            if file_serializer.is_valid():
                file_serializer.save()
                logging_conf.logger.debug("worked")
                soldier = self.gibushstats.values("soldier_id", "myun_id")[0]
                soldier["stage"] = self.stage
                #TODO
                #calculate_grades(soldier)
                return Response("חישב סוציומטרי של שלב", status=status.HTTP_201_CREATED)
            else:
                logging_conf.logger.debug("errors {}".format(file_serializer.errors))
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            pass


class ParticipentProfileView(APIView):
    """
    Description: API endpoint that allows filter data from participent table and gibush stats table - can handle with combinations of headers and values
    headers: everything
    """
    def get(self, request, *args, **kwargs):
        assessors = []
        soldier_id = request.GET.get("id")
        myun_id = request.GET.get("myun_id")
        participent_details = Participent.objects.filter(id=soldier_id)
        if myun_id:
            gibush_stats_details = GibushStats.objects.filter(soldier_id=soldier_id, myun_id=myun_id).values()
            grades_and_assessors = AssessorEvaluation.objects.filter(soldier_id=soldier_id, myun_id=myun_id).values()
        else:
            gibush_stats_details = GibushStats.objects.filter(soldier_id=soldier_id).values()
            grades_and_assessors = AssessorEvaluation.objects.filter(soldier_id=soldier_id).values()
        for assessor in grades_and_assessors:
            assessor_new = dict(assessor)
            assessor_names = Assessor.objects.filter(id=assessor["assessor_id"]).values("first_name", "last_name")[0]
            assessor_new["full_name"] = "{} {}".format(assessor_names["first_name"], assessor_names["last_name"])
            del assessor_new["assessor_id"]
            del assessor_new["myun_id"]
            del assessor_new["soldier_id"]
            del assessor_new["id"]
            assessors.append(assessor_new)

        dict_for_serializer = {}
        if gibush_stats_details:
            dict_for_serializer = gibush_stats_details[0].copy()
        
        dict_for_serializer["participent"] = dict(participent_details.values()[0])
        dict_for_serializer["assessors_grade_names"] = assessors
        file_serializer = ParticipentProfileSerializer([dict_for_serializer], many=True)
        logging_conf.logger.debug("return all data for paticipant number {}".format(soldier_id))
        return Response(file_serializer.data)
                   
class RetirementsView(viewsets.ModelViewSet):
    """
    Description: API endpoint that allows retire person and list all retirements
    get = list
        return first name, last name, identity number for all retirements 
    post=create
        scenario: retire participant from gibush
                when - action is not delete
                and - soldier in myun is not exists in retirements table
                then - add soldier in myun to retirements table with those fields: retirements reason, days in gibush, retirement stage.

        scenario: return retire participant to gibush
                when - action is delete
                then - update participant status ib GibushStats table to - "נקלט"
                and - delete soldier in myun from retirements table

        scenario: update retire participant from gibush
                when - action is not delete
                and - soldier in myun exists in retirements table
                then - update soldier in myun object in retirements table with those fields: retirements reason, days in gibush, retirement stage.

    """
    queryset = retirements.objects.all()
    serializer_class = CreateRetirementsSerializer
    def list(self, request, *args, **kwargs):
        request_dict = {}
        for key, value in request.GET.items():
            request_dict[key] = int(value[0])
        retirements_list = retirements.objects.filter(**request_dict).values()
        ids = retirements_list.values_list("soldier_id", flat=True)
        parrticipants = Participent.objects.filter(id__in=ids)
        if parrticipants.exists():
            for reirement_participent in retirements_list:
                participant_properties = Participent.objects.filter(id=reirement_participent["soldier_id"]).values('first_name', 'last_name', 'identity_number')
                reirement_participent.update(**participant_properties[0])
            file_serializer = ListRetirementsSerializer(retirements_list, many=True)
            logging_conf.logger.debug("return all retirements")
        else:
            logging_conf.logger.debug("no retirements has returned")
            return Response("no retirements")
        return Response(file_serializer.data)

    def create(self, request, *args, **kwargs):
        action = request.data.pop("action")
        if action == "delete":
            GibushStats.objects.filter(soldier_id=request.data["soldier_id"],myun_id=request.data["myun_id"]).update(participent_status="נקלט")
            retirements.objects.filter(soldier_id=request.data["soldier_id"]).delete()
            logging_conf.logger.debug("participant num {}, has reaccept to gibush and has deleted from retirements".format(request.data.get("soldier_id")))
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            participant_stat = retirements.objects.filter(soldier_id=request.data["soldier_id"])
            try:
                GibushStats.objects.filter(soldier_id=request.data["soldier_id"],myun_id=request.data["myun_id"]).update(participent_status="פרש")
            except:
                pass
            if "soldier_id" in list(request.data.keys()) and participant_stat.exists():
                participent_stat = retirements.objects.get(soldier_id=request.data["soldier_id"])
                file_serializer = CreateRetirementsSerializer(participant_stat, data=request.data, partial=True)
                logging_conf.logger.debug("retirement stats has been updated for participant num {}".format(request.data.get("soldier_id")))
            else:
                file_serializer = CreateRetirementsSerializer(data=request.data)
                logging_conf.logger.debug("participant num {}, has retire from gibush".format(request.data.get("soldier_id")))
            if file_serializer.is_valid():
                file_serializer.save()
                return Response("success", status=status.HTTP_201_CREATED)
            else:
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AllGradesView(APIView):
    """
    Description: API endpoint that allows return data from participent table, gibush stats table and assessorEvaluation table.
    returns all data about all the participants and place them by their final grade
    """
    def get(self, request, *args, **kwargs):
        x = []
        count = 0
        logging_conf.logger.debug("updating final rating")
        for id1 in GibushStats.objects.filter(myun_id=request.GET["myun_id"], participent_status="נקלט").order_by('-final_grade').values_list("soldier_id", flat=True):
            count += 1
            assessors = []
            participent_details = Participent.objects.filter(id=id1)
            gibush_stats_details = GibushStats.objects.filter(soldier_id=id1).values()
            grades_and_assessors = AssessorEvaluation.objects.filter(soldier_id=id1).values()
            gibush_stats_details.update(final_rating=count)
            for assessor in grades_and_assessors:
                assessor_new = dict(assessor)
                assessor_names = Assessor.objects.filter(id=assessor["assessor_id"]).values("first_name", "last_name")[0]
                assessor_new["full_name"] = "{} {}".format(assessor_names["first_name"], assessor_names["last_name"])
                del assessor_new["assessor_id"]
                del assessor_new["myun_id"]
                del assessor_new["soldier_id"]
                del assessor_new["id"]
                assessors.append(assessor_new)
            dict_for_serializer = gibush_stats_details[0].copy()
            dict_for_serializer["participent"] = dict(participent_details.values()[0])
            dict_for_serializer["assessors_grade_names"] = assessors
            x.append(dict_for_serializer)
        file_serializer = ParticipentProfileSerializer(x, many=True)
        logging_conf.logger.debug("return all grades of participants in gibush - sort by final grade")
        return Response(file_serializer.data)
    
class OutputGradesView(APIView):
    """
    Description: API endpoint that allows return csv file with data participent table, gibush stats table and assessorEvaluation table.
    returns all data about all the participants and place them by their final grade
    """
    def get(self, request, *args, **kwargs):
        x = []
        for id1 in GibushStats.objects.filter(myun_id=request.GET["myun_id"], participent_status="נקלט").order_by('-final_grade').values_list("soldier_id", flat=True):
            assessors = []
            participent_details = Participent.objects.filter(id=id1)
            gibush_stats_details = GibushStats.objects.filter(soldier_id=id1).values()
            grades_and_assessors = AssessorEvaluation.objects.filter(soldier_id=id1).values()
            for assessor in grades_and_assessors:
                assessor_new = dict(assessor)
                assessor_names = Assessor.objects.filter(id=assessor["assessor_id"]).values("first_name", "last_name")[0]
                assessor_new["full_name"] = "{} {}".format(assessor_names["first_name"], assessor_names["last_name"])
                del assessor_new["assessor_id"]
                del assessor_new["myun_id"]
                del assessor_new["soldier_id"]
                del assessor_new["id"]
                assessors.append(assessor_new)
            dict_for_serializer = gibush_stats_details[0].copy()
            dict_for_serializer["participent"] = dict(participent_details.values()[0])
            dict_for_serializer["assessors_grade_names"] = assessors
            x.append(dict_for_serializer)
        response = HttpResponse()
        response['Content-Type'] ='text/csv'
        response['Content-Disposition'] = 'attachment; filename=final_grades.csv'
        response.write(u'\ufeff'.encode('utf8'))
        response = csv_output.final_grades_csv(x, response)
        logging_conf.logger.debug("final grades csv has created and sended to client")
        return response


class OutputGroupsView(APIView):
    """
    API endpoint that return csv file of participant and their groups(first name, last name, identity number, group), sort by group
    """
    def get(self, request, *args, **kwargs):
        myun_id = request.GET["myun_id"]
        stage = request.GET["stage"]
        group_id = request.GET["group_id"]
        names = []
        if stage == '1':
            try:
                if int(group_id):
                    soldiers = GibushStats.objects.filter(first_stage_group=group_id, myun_id=myun_id).values_list("soldier_id", flat=True)
                    names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name", "identity_number")
                    for name in names_per_group:
                        name["group_id"] = group_id
                    names.append(list(names_per_group))
            except:
                groups = GibushGroup.objects.filter(myun_id=myun_id, stage=stage).values_list("group_id", flat=True)
                for group in groups:
                    soldiers = GibushStats.objects.filter(first_stage_group=group, myun_id=myun_id).values_list("soldier_id", flat=True)
                    names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name", "identity_number")
                    for name in names_per_group:
                        name["group_id"] = group
                    names.append(list(names_per_group))
        elif stage == '2':
            try:
                if int(group_id):
                    soldiers = GibushStats.objects.filter(second_stage_group=group_id, myun_id=myun_id).values_list("soldier_id", flat=True)
                    names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name", "identity_number")
                    for name in names_per_group:

                        name["group_id"] = group_id
                    name.append(list(names_per_group))
            except:
                groups = GibushGroup.objects.filter(myun_id=myun_id, stage=stage).values_list("group_id", flat=True)
                for group in groups:
                    soldiers = GibushStats.objects.filter(second_stage_group=group, myun_id=myun_id).values_list("soldier_id", flat=True)
                    names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name", "identity_number")
                    for name in names_per_group:
                        name["group_id"] = group
                    names.append(list(names_per_group))
        response = HttpResponse()
        response['Content-Type'] ='text/csv'
        response['Content-Disposition'] = 'attachment; filename=groups.csv'
        response.write(u'\ufeff'.encode('utf8'))
        response = csv_output.groups_csv(names, response)
        logging_conf.logger.debug("participants per group csv has created and sended to client")
        return response

def render_to_pdf(context_dict):
    """
    render sozio for participants - render data into pdf file and archive it into zip
    """
    template = get_template('mytemplate.html')
    in_memory = BytesIO()
    zip = ZipFile(in_memory, "a")
    for index in range(len(context_dict)):
        html  = template.render(context_dict[index])
        result = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result, encoding='UTF-8', html_encoding='UTF-8')
        if not pdf.err:
            zip.writestr("{}.pdf".format(str(index)), result.getvalue())
    zip.close()
    response = HttpResponse()
    response['Content-Type'] ='application/zip'
    response["Content-Disposition"] = "attachment; filename=sozio_documents.zip"
    in_memory.seek(0)    
    response.write(in_memory.read())
    logging_conf.logger.debug("sozio files for participant filling pdf has created and sended to client")
    return response

def ReturnSozioFiles(request):
    """
    API endpoint that gets group/all groups and send data into render to pdf, to create pdf of all participant in group,
    or create pdfs of all groups and their participants, and archive it to zip
    """
    myun_id = request.GET["myun_id"]
    stage = request.GET["stage"]
    group_id = request.GET["group_id"]
    names = []
    if stage == '1':
        try:
            if int(group_id):
                soldiers = GibushStats.objects.filter(first_stage_group=group_id, myun_id=myun_id).values_list("soldier_id", flat=True)
                names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name")
                names_per_group = prepare_dict(names_per_group, "mylist")
                names.append(names_per_group)
        except:
            groups = GibushGroup.objects.filter(myun_id=myun_id, stage=stage).values_list("group_id", flat=True)
            for group in groups:
                soldiers = GibushStats.objects.filter(first_stage_group=group, myun_id=myun_id).values_list("soldier_id", flat=True)
                names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name")
                names_per_group = prepare_dict(names_per_group, "mylist")
                names.append(names_per_group)
    elif stage == '2':
        try:
            if int(group_id):
                soldiers = GibushStats.objects.filter(second_stage_group=group_id, myun_id=myun_id).values_list("soldier_id", flat=True)
                names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name")
                names_per_group = prepare_dict(names_per_group, "mylist")
                names.append(names_per_group)
        except:
            groups = GibushGroup.objects.filter(myun_id=myun_id, stage=stage).values_list("group_id", flat=True)
            for group in groups:
                soldiers = GibushStats.objects.filter(second_stage_group=group, myun_id=myun_id).values_list("soldier_id", flat=True)
                names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name")
                names_per_group = prepare_dict(names_per_group, "mylist")
                names.append(names_per_group)
    return render_to_pdf(names)
    

def ReturnAssessorEvaluationFiles(request):
    """
    API endpoint that gets assessors and participants from group and send data into render to pdf, and archive it to zip
    """
    myun_id = request.GET["myun_id"]
    stage = request.GET["stage"]
    group_id = request.GET["group_id"]
    assessors_in_group = AssessorInGroup.objects.filter(myun_id=myun_id, stage=stage, group_id=group_id).values_list("assessor_id", flat=True)
    assessors_names = Assessor.objects.filter(id__in=assessors_in_group).values("first_name", "last_name")
    assessors_names = prepare_dict_assessors(assessors_names, "assessor")
    names = []
    if stage == '1':
        if int(group_id):
            soldiers = GibushStats.objects.filter(first_stage_group=group_id, myun_id=myun_id).values_list("soldier_id", flat=True)
            names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name")
            names_per_group = prepare_dict_assessors(names_per_group, "mylist")
    elif stage == '2':
        if int(group_id):
            soldiers = GibushStats.objects.filter(second_stage_group=group_id, myun_id=myun_id).values_list("soldier_id", flat=True)
            names_per_group = Participent.objects.filter(id__in=soldiers).values("first_name", "last_name")
            names_per_group = prepare_dict_assessors(names_per_group, "mylist")
    return render_to_pdf_assessors(names_per_group, assessors_names, group_id)
    
def render_to_pdf_assessors(context_dict, assessor_names, group_id):
    template = get_template('AssessorEvaluationTemplate.html')
    in_memory = BytesIO()
    zip = ZipFile(in_memory, "a")
    for participent in context_dict:
        for assessor in assessor_names:
            logging_conf.logger.debug(str(assessor))
            logging_conf.logger.debug(str(participent))
            html  = template.render({"participant": participent, "assessor": assessor, "group": group_id})
            result = BytesIO()
            pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result, encoding='UTF-8', html_encoding='UTF-8')
            if not pdf.err:
                zip.writestr("מגבש_{}_{}_מלשב_{}_{}_קבוצה_{}.pdf".format(str(assessor["first_name"][::-1]),str(assessor["last_name"][::-1]),str(participent["first_name"][::-1]), str(participent["last_name"][::-1]), group_id), result.getvalue())
    zip.close()
    response = HttpResponse()
    response['Content-Type'] ='application/zip'
    response["Content-Disposition"] = "attachment; filename=sozio_documents.zip"
    in_memory.seek(0)    
    response.write(in_memory.read())
    logging_conf.logger.debug("sozio files for participant filling pdf has created and sended to client")
    return response

def prepare_dict(names, list_name):
    """
    handle with problem of hebraw in pdf(up side down hebraw words) for sozio
    """
    x = {}
    for index in range(len(names)):
        names[index]['first_name'] = names[index]['first_name'][::-1]
        names[index]['last_name'] =  names[index]['last_name'][::-1]
    x[list_name] = list(names)
    print(x)
    return x


def prepare_dict_assessors(names, list_name):
    """
    handle with problem of hebraw in pdf(up side down hebraw words) for assessors
    """
    x = {}
    for index in range(len(names)):
        names[index]['first_name'] = names[index]['first_name'][::-1]
        names[index]['last_name'] =  names[index]['last_name'][::-1]
    print(x)
    return names
class ReturnParticipentListSpecificGibush(APIView):
    """
    Description: API endpoint that allows filter data from participent table and gibush stats table - can handle with combinations of headers and values
    headers: everything
    """
    def get(self, request, *args, **kwargs):
        participent_fields = Participent._meta.fields
        gibush_stats_fields = GibushStats._meta.fields
        participent_fields_list = [participent.name for participent in participent_fields]
        gibush_stats_fields_list = [stat.name for stat in gibush_stats_fields]
        request_dict_participent = {}
        request_dict_gibushstats = {}
        myun_soldiers = GibushStats.objects.filter(participent_status="נקלט")
        for key, value in request.GET.items():
            if key in participent_fields_list: 
                request_dict_participent[key] = value
            elif key in gibush_stats_fields_list and key != "id":
                request_dict_gibushstats[key] = value
            else:
                if key == "stage":
                    if value == '1':
                        myun_soldiers = GibushStats.objects.filter(first_stage_group=None, participent_status="נקלט").values_list("soldier_id", flat=True)
                        myun_soldiers = GibushStats.objects.all().exclude(soldier_id__in=myun_soldiers)
                    else:
                        myun_soldiers = GibushStats.objects.filter(second_stage_group=None, participent_status="נקלט").values_list("soldier_id", flat=True)
                        myun_soldiers = GibushStats.objects.all().exclude(soldier_id__in=myun_soldiers)
        try:
            myun_soldiers = myun_soldiers.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        except:
            #request_dict_gibushstats["participent_status"] = "נקלט"
            myun_soldiers = GibushStats.objects.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        queryset = Participent.objects.filter(id__in=myun_soldiers, **request_dict_participent)
        file_serializer = ParticipentSerializer(queryset, many=True)
        return Response(file_serializer.data)

class ReturnParticipentListEveryOne(APIView):
    """
    Description: API endpoint that allows filter data from participent table and gibush stats table - can handle with combinations of headers and values
    headers: everything
    """
    def get(self, request, *args, **kwargs):
        participent_fields = Participent._meta.fields
        gibush_stats_fields = GibushStats._meta.fields
        participent_fields_list = [participent.name for participent in participent_fields]
        gibush_stats_fields_list = [stat.name for stat in gibush_stats_fields]
        request_dict_participent = {}
        request_dict_gibushstats = {}
        myun_soldiers = GibushStats.objects.all()
        for key, value in request.GET.items():
            if key in participent_fields_list: 
                request_dict_participent[key] = value
            elif key in gibush_stats_fields_list and key != "id":
                request_dict_gibushstats[key] = value
        try:
            myun_soldiers = myun_soldiers.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        except:
            #request_dict_gibushstats["participent_status"] = "נקלט"
            myun_soldiers = GibushStats.objects.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        queryset = Participent.objects.filter(id__in=myun_soldiers, **request_dict_participent)
        file_serializer = ParticipentSerializer(queryset, many=True)
        return Response(file_serializer.data)

class SozioGibsuhGradesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioGibushGrades.objects.all()
    serializer_class = SozioGibushGradesSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        request_dict = {}
        for key, value in request.GET.items():
            request_dict[key] = int(value[0])
        queryset = SozioGibushGrades.objects.filter(**request_dict)
        file_serializer = SozioGibushGradesSerializer(queryset, many=True)
        return Response(file_serializer.data)


class ImportGibush(APIView):
    def post(self, request, *args, **kwargs):
        json_file = request.data.get('json_file')
        data = json.loads(json_file.read().decode())
        participants = data.get("api.participant")
        assessors = data.get("api.assessor")
        gibush = data.get("api.gibush")
        gibush_file = GibushSerializer(data=gibush)
        participant_file = ParticipentSerializer(data=participants, many=True)
        if gibush_file.is_valid():
            gibush_file.save()
            if participant_file.is_valid():
                participant_file.save()
                assessor_file = AssessorSerializer(data=assessors, many=True)
                if assessor_file.is_valid():
                    assessor_file.save()
                    return Response("success", status=status.HTTP_201_CREATED)
                else:
                    pass
            else:
                return Response(participant_file.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            return Response(gibush_file.errors, status=status.HTTP_400_BAD_REQUEST)


class ExportGibush(APIView):
    def get(self, request, *args, **kwargs):
        app_models = apps.get_app_config('api').get_models()
        print(app_models)
        json = {}
        for model in app_models:
            print(model.__name__)
            json[model.__name__] = list(model.objects.all().values())
        #gibush = request.GET.get("gibush")
        return Response(json, status=status.HTTP_200_OK)
   