import json
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics, status
from django.db.models import F, Q, FilteredRelation,  BooleanField, Case, When, Value, IntegerField
from django.conf import settings
from api.route.serializers import SozioRouteGradesPostSerializer, SozioRouteGradesListSerializer, TeamListSerializer, TeamCreateSerializer, RouteSoldierSerializer, TeamSoldiersSerializer, CheckedDocumentsSerializer, SozioRouteTrustWorthyPostSerializer, SozioRouteTrustWorthyListSerializer
from api.models import SozioRouteGrades, Team, RouteSoldier, SozioRouteTrustWorthy
from rest_framework.views import APIView
from rest_framework.response import Response
import csv
import os
from django.conf import settings
from io import BytesIO
from django.http import HttpResponse
from api.assets import logging_conf
import sys

MEDIA_ROOT = settings.MEDIA_ROOT

TABLE_FILE_HEADERS = {'שם פרטי': 'first_name',
                      'שם משפחה': 'last_name',
                      'תז': 'identity_number',  
                      'מספר אישי': 'personal_number',
                      'תאריך לידה': 'birth_date',
                      'טלפון 1': 'first_phone',
                      'טלפון 2': 'second_phone',
                      'טלפון 3': 'third_phone',
                      'כתובת': 'address',
                      'עולה חדש': 'is_new_immigrant',  
                      'סטטוס מלשביות': 'enlistment_status',
                      'קבא': 'kabha',
                      'דפר': 'dapar',
                      'צדכ': 'tzadach',
                      'פרופיל': 'medical_profile',
                      'סעיף רפואי פוסל': 'disqualifying_medical_trait'}


class RouteSoldierViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = RouteSoldier.objects.all()
    serializer_class = RouteSoldierSerializer
    def list(self, request, *args, **kwargs):
        request_dict = {}
        for key, value in request.GET.items():
            request_dict[key] = int(value[0])
        print(request_dict)
        queryset = RouteSoldier.objects.filter(**request_dict)
        file_serializer = RouteSoldierSerializer(queryset, many=True)
        return Response(file_serializer.data)
    
    def create(self, request, *args, **kwargs):
        participant_stat = RouteSoldier.objects.filter(soldier_id=request.data["soldier"]).first()
        if "soldier" in list(request.data.keys()) and participant_stat:
            print(request.data)
            file_serializer = RouteSoldierSerializer(participant_stat, data=request.data, partial=True)
        else:
            file_serializer = RouteSoldierSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamSoldiersViewSet(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSoldiersSerializer

class TeamViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows gibush to be viewed or edited.
    """
    queryset = Team.objects.all()
    serializer_class = TeamListSerializer

    """
    Description: API endpoint that allows create some gibushim and list al the gibushim
    """
    def create(self, request, *args, **kwargs):
        file_serializer = TeamCreateSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ReturnParticipentListSpecificGibush(APIView):
    """
    Description: API endpoint that allows filter data from participent table and gibush stats table - can handle with combinations of headers and values
    headers: everything
    """
    def get(self, request, *args, **kwargs):
        participent_fields = Participent._meta.fields
        gibush_stats_fields = GibushStats._meta.fields
        participent_fields_list = [participent.name for participent in participent_fields]
        gibush_stats_fields_list = [stat.name for stat in gibush_stats_fields]
        request_dict_participent = {}
        request_dict_gibushstats = {}
        myun_soldiers = GibushStats.objects.filter(participent_status="נקלט")
        for key, value in request.GET.items():
            if key in participent_fields_list: 
                request_dict_participent[key] = value
            elif key in gibush_stats_fields_list and key != "id":
                request_dict_gibushstats[key] = value
            else:
                if key == "stage":
                    if value == '1':
                        myun_soldiers = GibushStats.objects.filter(first_stage_group=None, participent_status="נקלט").values_list("soldier_id", flat=True)
                        myun_soldiers = GibushStats.objects.all().exclude(soldier_id__in=myun_soldiers)
                    else:
                        myun_soldiers = GibushStats.objects.filter(second_stage_group=None, participent_status="נקלט").values_list("soldier_id", flat=True)
                        myun_soldiers = GibushStats.objects.all().exclude(soldier_id__in=myun_soldiers)
        try:
            myun_soldiers = myun_soldiers.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        except:
            #request_dict_gibushstats["participent_status"] = "נקלט"
            myun_soldiers = GibushStats.objects.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        queryset = Participent.objects.filter(id__in=myun_soldiers, **request_dict_participent)
        file_serializer = ParticipentSerializer(queryset, many=True)
        return Response(file_serializer.data)

class ReturnParticipentListEveryOne(APIView):
    """
    Description: API endpoint that allows filter data from participent table and gibush stats table - can handle with combinations of headers and values
    headers: everything
    """
    def get(self, request, *args, **kwargs):
        participent_fields = Participent._meta.fields
        gibush_stats_fields = GibushStats._meta.fields
        participent_fields_list = [participent.name for participent in participent_fields]
        gibush_stats_fields_list = [stat.name for stat in gibush_stats_fields]
        request_dict_participent = {}
        request_dict_gibushstats = {}
        myun_soldiers = GibushStats.objects.all()
        for key, value in request.GET.items():
            if key in participent_fields_list: 
                request_dict_participent[key] = value
            elif key in gibush_stats_fields_list and key != "id":
                request_dict_gibushstats[key] = value
        try:
            myun_soldiers = myun_soldiers.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        except:
            #request_dict_gibushstats["participent_status"] = "נקלט"
            myun_soldiers = GibushStats.objects.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        queryset = Participent.objects.filter(id__in=myun_soldiers, **request_dict_participent)
        file_serializer = ParticipentSerializer(queryset, many=True)
        return Response(file_serializer.data)


class FilledDocuments(APIView):
    def get(self, request, *args, **kwargs):
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        route_soldiers = RouteSoldier.objects.filter(team_id=team_id)
        route_filled_documents = list(route_soldiers.filter(grades_filled__sozio_stage=sozio_stage).values_list('grades_filled__document_number', flat=True))
        dict_route_soldiers = list(route_soldiers.values())
        for i in range(1, len(dict_route_soldiers) + 1):
            if i in route_filled_documents:
                dict_route_soldiers[i-1]["check"] = True
            else:
                dict_route_soldiers[i-1]["check"] = False
        file_serializer = CheckedDocumentsSerializer(dict_route_soldiers, many=True)
        return Response(file_serializer.data)


class SozioRouteTrustWorthyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteTrustWorthy.objects.all()
    serializer_class = SozioRouteGradesListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        request_dict = {}
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        soldiers = RouteSoldier.objects.filter(team_id=team_id).values_list('id', flat=True)
        queryset = SozioRouteTrustWorthy.objects.filter(subject_participent_id__in=soldiers, sozio_stage=sozio_stage, document_number=document_number)
        if not queryset.exists():
            queryset = RouteSoldier.objects.filter(team_id=team_id)
            file_serializer = RouteSoldierSerializer(queryset, many=True)
        else:
            file_serializer = SozioRouteGradesListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):

        keys_for_update = ["document_number", "subject_participent_id", "sozio_stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioRouteTrustWorthy.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioRouteTrustWorthyPostSerializer(grades, data=sozio_dict, partial=True)
            else:
                file_serializer = SozioRouteTrustWorthyPostSerializer(data=sozio_dict)
            if file_serializer.is_valid():
                file_serializer.save()
                """
                self.subject_id = sozio_dict["subject_participent_id"]
                self.sozio_type = sozio_dict["sozio_stage"]
                self.by_subject = SozioRouteGrades.objects.filter(subject_participent_id=self.subject_id, sozio_stage=self.sozio_type)
                self.was_subject = self.by_subject.count()
                self.RouteSoldier = RouteStats.objects.filter(soldier_id=self.subject_id)
                self.group = self.RouteSoldier.values("team_id")
                self.people_in_group = RouteStats.objects.filter(team_id=self.group[0]["team_id"]).count() - 1
                result = self.calculate_sozio()
                """
            else:
                print(file_serializer.errors)
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)

class SozioRouteGradesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteGrades.objects.all()
    serializer_class = SozioRouteGradesListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.

        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        request_dict = {}
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        soldiers = RouteSoldier.objects.filter(team_id=team_id).values_list('id', flat=True)
        queryset = SozioRouteGrades.objects.filter(subject_participent_id__in=soldiers, sozio_stage=sozio_stage, document_number=document_number)
        if not queryset.exists():
            queryset = RouteSoldier.objects.filter(team_id=team_id)
            file_serializer = RouteSoldierSerializer(queryset, many=True)
        else:
            file_serializer = SozioRouteGradesListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):

        keys_for_update = ["document_number", "subject_participent_id", "sozio_stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioRouteGrades.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioRouteGradesPostSerializer(grades, data=sozio_dict, partial=True)
                print(1)
            else:
                file_serializer = SozioRouteGradesPostSerializer(data=sozio_dict)
                print(2)
            if file_serializer.is_valid():
                file_serializer.save()
                """
                self.subject_id = sozio_dict["subject_participent_id"]
                self.sozio_type = sozio_dict["sozio_stage"]
                self.by_subject = SozioRouteGrades.objects.filter(subject_participent_id=self.subject_id, sozio_stage=self.sozio_type)
                self.was_subject = self.by_subject.count()
                self.RouteSoldier = RouteStats.objects.filter(soldier_id=self.subject_id)
                self.group = self.RouteSoldier.values("team_id")
                self.people_in_group = RouteStats.objects.filter(team_id=self.group[0]["team_id"]).count() - 1
                result = self.calculate_sozio()
                """
            else:
                print(file_serializer.errors)
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)

    # r=100-(100/((p-1)^{2}-p))*(SUM-(p-1))))
    def calculate_sozio(self):
        if self.was_subject == self.people_in_group:
            self.grades = self.by_subject.values_list("grade", flat=True)
            p = self.was_subject
            result = 100 - ((100 / ((p ** 2) - p)) * (sum(self.grades) - p))
            return float(result)
        else:
            return None


class export_participant(APIView):
    def get(self, request, *args, **kwargs):
        gibush = request.GET.get("gibush")
        contents = os.listdir(settings.MEDIA_ROOT)
        filesdir = []
        for content in contents:
            if len(content.split("-")) == 3 and (int(content.split("-")[0]) - int(datetime.datetime.now().year) <= 3):
                files = os.listdir(os.path.join(settings.MEDIA_ROOT, content))
                print(files)
                filesdir.append([(os.path.join(settings.MEDIA_ROOT, content, item)) for item in files])
        print(filesdir)
        flat_list = [item.replace("\\", "/") for sublist in filesdir for item in sublist]
        print(flat_list)
        queryset = Participent.objects.filter(source_file__in=flat_list)
        assessors = Assessor.objects.all()
        gibush = Gibush.objects.get(pk=gibush)
        assessor_serializer = AssessorSerializer(assessors, many=True)
        participant_serializer = ParticipentExportSerializer(queryset, many=True)
        gibush_serializer = GibushSerializer(gibush)
        return Response({'api.participant':participant_serializer.data, 'api.assessor':assessor_serializer.data, 'api.gibush':gibush_serializer.data}, status=status.HTTP_200_OK)


class ImportGibush(APIView):
    def post(self, request, *args, **kwargs):
        json_file = request.data.get('file')
        data = json.loads(json_file.read().decode())
        for model, model_data in list(data.items()):
            model_obj = getattr(sys.modules[__name__], model)
            print(model_obj)
            for db_instance in model_data:
                if db_instance.get('id'):
                    instance = model_obj.objects.filter(id=db_instance.get('id'))
                    if instance.exists():
                        instance.update(**db_instance)
                    else:
                        print(db_instance, "itay")
                        model_obj.objects.create(**db_instance)
                elif db_instance.get('pk'):
                    instance = model_obj.objects.filter(pk=db_instance.get('pk'))
                    if instance.exists():
                        instance.update(**db_instance)
                    else:
                        model_obj.objects.create(**db_instance)
                else:
                    model_obj.objects.create(**db_instance)
        return Response("success", status=status.HTTP_201_CREATED)