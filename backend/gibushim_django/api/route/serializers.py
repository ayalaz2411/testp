from django.contrib.auth.models import User, Group
from api.models import SozioRouteGrades, Team, RouteSoldier, SozioRouteTrustWorthy
from api.gibush.serializers import ParticipentSerializer
from rest_framework import serializers
from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator
from django.core.files.base import ContentFile
from gibushim_django import settings
import os
from django.conf.urls.static import static

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class TeamListSerializer(serializers.ModelSerializer):
    commander_name = serializers.SerializerMethodField()
    class Meta:
        model = Team
        fields =   ['id', 'name', 'commander_id', 'commander_name']
    
    def get_commander_name(self, instance):
        if instance.commander:
            return "{} {}".format(instance.commander.first_name, instance.commander.last_name)
    
class TeamCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields =   ['id', 'name', 'commander']


class RouteSoldierSerializer(serializers.ModelSerializer):
    soldier_name = serializers.SerializerMethodField()
    team_name = serializers.SerializerMethodField()
    class Meta:
        model = RouteSoldier
        fields =   ['id', 'soldier', 'soldier_name', 'team', 'team_name']
    
    def get_soldier_name(self, instance):
        if instance.soldier:
            return "{} {}".format(instance.soldier.first_name, instance.soldier.last_name)

    def get_team_name(self, instance):
        if instance.team:
            return instance.team.name

class RouteSoldierParticipentPropertiesSerializer(serializers.ModelSerializer):
    soldier_values = serializers.SerializerMethodField()
    class Meta:
        model = RouteSoldier
        fields =   ['id', 'soldier_values']

    def get_soldier_values(self, instance):
        if instance.soldier:
            serializer = ParticipentSerializer(instance.soldier)
            return serializer.data


class TeamSoldiersSerializer(serializers.ModelSerializer):
    commander_name = serializers.SerializerMethodField()
    team_soldiers = RouteSoldierParticipentPropertiesSerializer(many=True, read_only=True)
    class Meta:
        model = Team
        fields =   ['id', 'name', 'commander_id', 'commander_name', 'team_soldiers']
    
    def get_commander_name(self, instance):
        if instance.commander:
            return "{} {}".format(instance.commander.first_name, instance.commander.last_name)


class CheckedDocumentsSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    check = serializers.BooleanField()
    
    class Meta:
        ordering = ["id",]



class SozioRouteGradesListSerializer(serializers.HyperlinkedModelSerializer):
    subject_participent_id = RouteSoldierSerializer(read_only=True)
    comment = serializers.CharField(max_length=1000, required=False)
    class Meta:
        model = SozioRouteGrades
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade",
                    "comment"] 


class SozioRouteTrustWorthyListSerializer(serializers.HyperlinkedModelSerializer):
    subject_participent_id = RouteSoldierSerializer(read_only=True)
    comment = serializers.CharField(max_length=1000, required=False)
    class Meta:
        model = SozioRouteTrustWorthy
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade",
                    "comment"] 

class SozioRouteTrustWorthyPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteTrustWorthy
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade",
                    "comment"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteTrustWorthy.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class SozioRouteGradesPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteGrades
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteGrades.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)

