from django.contrib.auth.models import User, Group
from api.models import Team, RouteSoldier, RouteRetirements, CommanderAssessments, Commanders, Participent
from api.gibush.serializers import ParticipentSerializer
from django.db.models import F, Q
from rest_framework import serializers
from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator
from django.core.files.base import ContentFile
from gibushim_django import settings
import os
from django.conf.urls.static import static

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class TeamListSerializer(serializers.ModelSerializer):
    commander_name = serializers.SerializerMethodField()
    class Meta:
        model = Team
        fields =   ['id', 'name', 'commander_id', 'commander_name']
    
    def get_commander_name(self, instance):
        if instance.commander:
            return "{} {}".format(instance.commander.commander_properties.first_name, instance.commander.commander_properties.last_name)
    
class TeamCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields =   ['id', 'name', 'commander']



class RouteSoldierSerializer(serializers.ModelSerializer):
    soldier_name = serializers.SerializerMethodField()
    team_name = serializers.SerializerMethodField()
    class Meta:
        model = RouteSoldier
        fields =   ['id', 'soldier', 'soldier_name', 'team', 'team_name']
    
    def get_soldier_name(self, instance):
        if instance.soldier:
            return "{} {}".format(instance.soldier.first_name, instance.soldier.last_name)

    def get_team_name(self, instance):
        if instance.team:
            return instance.team.name


class RouteSoldierForRetirementsSerializer(RouteSoldierSerializer):
    identity_number = serializers.SerializerMethodField()

    class Meta:
        model = RouteSoldier
        fields = ['id', 'soldier', 'soldier_name', 'team', 'team_name', 'identity_number']

    def get_identity_number(self, instance):
        if instance.soldier:
            return instance.soldier.identity_number

class RouteSoldierParticipentPropertiesSerializer(serializers.ModelSerializer):
    checked = serializers.BooleanField(required=False)
    soldier_values = serializers.SerializerMethodField()
    class Meta:
        model = RouteSoldier
        fields =   ['id', 'soldier_values', 'checked', 'status']

    def get_soldier_values(self, instance):
        if instance.status != "הודח":
            try:
                serializer = ParticipentSerializer(instance.get('soldier'))
            except:
                serializer = ParticipentSerializer(instance.soldier)
            return serializer.data

class TeamSoldiersSerializer(serializers.ModelSerializer):
    commander_name = serializers.SerializerMethodField()
    team_soldiers = serializers.SerializerMethodField()
    class Meta:
        model = Team
        fields =   ['id', 'name', 'commander_id', 'commander_name', 'team_soldiers']
    
    def get_commander_name(self, instance):
        if instance.commander:
            return "{} {}".format(instance.commander.commander_properties.first_name, instance.commander.commander_properties.last_name)

    def get_team_soldiers(self, instance):
        queryset = RouteSoldier.objects.filter(~Q(status='הודח'), team_id=instance.id)
        file_serializer = RouteSoldierParticipentPropertiesSerializer(queryset, many=True, read_only=True)
        return file_serializer.data

class CreateRetirementsSerializer(serializers.ModelSerializer):

    class Meta:
        model = RouteRetirements
        fields = ['soldier',
                'retirements_reason',
                'retirement_stage']

    def create(self, validated_data):
        print(validated_data)
        return RouteRetirements.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class RouteSoldierRetiremetnsParticipentPropertiesSerializer(serializers.ModelSerializer):
    checked = serializers.BooleanField(required=False)
    soldier_values = ParticipentSerializer(read_only=True)
    class Meta:
        model = RouteSoldier
        fields =   ['id', 'soldier_values', 'checked', 'status']
    

class ListRetirementsSerializer(serializers.ModelSerializer):
    soldier = RouteSoldierForRetirementsSerializer(read_only=True)

    class Meta:
        model = RouteRetirements
        fields=['soldier',
                'soldier_id',
                'retirements_reason',
                'retirement_date',
                'retirement_stage']

class CommanderSerializer(serializers.ModelSerializer):
    commander_properties = ParticipentSerializer(read_only=True)
    class Meta:
        model = Commanders
        fields = ['commander_properties', 'start_date']

class ReturnAllAssociatedModelsOfParticipantSerializer(serializers.Serializer):
    attribute = serializers.ChoiceField(['myun_id', 'team_id'])
    value = serializers.IntegerField()
    class Meta:
        fields = ['attribute', 'value']

class CommanderAssessmentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommanderAssessments
        fields =   ['sozio_stage', 'soldier', 'commander', 'grade', 'comment']

    def create(self, validated_data):
        print(validated_data)
        return CommanderAssessments.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class RouteParticipantProfileSerializer(serializers.Serializer):
    team = serializers.SerializerMethodField()
    soldier = serializers.SerializerMethodField()
    commander_assessment = serializers.SerializerMethodField()
    id = serializers.IntegerField(read_only=True)
    user_trustworthy_avg = serializers.IntegerField(allow_null=True)
    user_stress_avg = serializers.IntegerField(allow_null=True)
    total_soziometry = serializers.IntegerField(allow_null=True)
    total_fit_commander = serializers.IntegerField(allow_null=True)
    total_not_match = serializers.IntegerField(allow_null=True)
    total_friendship = serializers.IntegerField(allow_null=True)
    not_match_place = serializers.IntegerField(allow_null=True)
    friendship_place = serializers.IntegerField(allow_null=True)
    fit_commander_place = serializers.IntegerField(allow_null=True)
    soziometry_place = serializers.IntegerField(allow_null=True)
    trustworthy_place = serializers.IntegerField(allow_null=True)
    stress_place = serializers.IntegerField(allow_null=True)
    
    class Meta:
        fields =   ['id',
                    'user_stress_avg',
                    'user_trustworthy_avg',
                    'total_fit_commander',
                    'total_soziometry',
                    'total_not_match',
                    'total_friendship',
                    'team',
                    'commander_assessment',
                    'not_match_place',
                    'friendship_place',
                    'fit_commander_place',
                    'soziometry_place',
                    'trustworthy_place',
                    'stress_place',
                    'soldier']

    def get_team(self, instance):
        team = Team.objects.get(id=instance.get("team"))
        if team:
            return TeamListSerializer(team, read_only=True).data 

    def get_soldier(self, instance):
        soldier = Participent.objects.get(id=instance.get("soldier"))
        if soldier:
            return ParticipentSerializer(soldier, read_only=True).data

    def get_commander_assessment(self, instance):
        team = Team.objects.get(id=instance.get("team"))
        assessments = CommanderAssessments.objects.filter(soldier_id=instance.get("id"), commander_id=team.commander.id)
        if assessments:
            return CommanderAssessmentsSerializer(assessments, many=True, read_only=True).data