import json
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics, status
from django.db.models import F, Q, FilteredRelation,  BooleanField, Case, When, Value, IntegerField
from django.conf import settings
from api.models import *
from api.gibush.serializers import AssessorSerializer, ParticipentExportSerializer, GibushSerializer, MarkArrivedParticipentSerializer
from api.route.sozio.serializers import SozioRouteListSerializer
from api.route.routesoldiers.serializers import TeamListSerializer, TeamCreateSerializer, RouteSoldierSerializer, TeamSoldiersSerializer, ListRetirementsSerializer, CreateRetirementsSerializer, CommanderAssessmentsSerializer, ReturnAllAssociatedModelsOfParticipantSerializer, RouteParticipantProfileSerializer
from api.models import Team, RouteSoldier, RouteRetirements, CommanderAssessments, Commanders, Participent, GibushStats, FieldDaysStats, Assessor, Gibush
from api.route.sozio.views import calculate_final_grades_all_stages
from rest_framework.views import APIView
from rest_framework.response import Response
import csv
import os
from django.conf import settings
from io import BytesIO
from django.http import HttpResponse
from api import logging_conf
import sys
import datetime

MEDIA_ROOT = settings.MEDIA_ROOT

TABLE_FILE_HEADERS = {'שם פרטי': 'first_name',
                      'שם משפחה': 'last_name',
                      'תז': 'identity_number',  
                      'מספר אישי': 'personal_number',
                      'תאריך לידה': 'birth_date',
                      'טלפון 1': 'first_phone',
                      'טלפון 2': 'second_phone',
                      'טלפון 3': 'third_phone',
                      'כתובת': 'address',
                      'עולה חדש': 'is_new_immigrant',  
                      'סטטוס מלשביות': 'enlistment_status',
                      'קבא': 'kabha',
                      'דפר': 'dapar',
                      'צדכ': 'tzadach',
                      'פרופיל': 'medical_profile',
                      'סעיף רפואי פוסל': 'disqualifying_medical_trait'}

class JoinToTeamParticipantOptions(APIView):
    def get(self, request, *args, **kwargs):
        team_id = request.GET.get("team_id")
        queryset = Participent.objects.filter(Q(routers__retirements__retirements_reason="רפואי") | Q(routers__isnull=True))
        file_serializer = MarkArrivedParticipentSerializer(queryset, many=True)
        return Response (file_serializer.data)


class RouteSoldierViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = RouteSoldier.objects.all()
    serializer_class = RouteSoldierSerializer
    def list(self, request, *args, **kwargs):
        request_dict = {}
        for key, value in request.GET.items():
            request_dict[key] = int(value[0])
        queryset = RouteSoldier.objects.filter(**request_dict)
        file_serializer = RouteSoldierSerializer(queryset, many=True)
        return Response(file_serializer.data)
    
    def create(self, request, *args, **kwargs):
        participant_stat = RouteSoldier.objects.filter(soldier_id=request.data["soldier"]).first()
        if "soldier" in list(request.data.keys()) and participant_stat:
            file_serializer = RouteSoldierSerializer(participant_stat, data=request.data, partial=True)
        else:
            file_serializer = RouteSoldierSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamSoldiersViewSet(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSoldiersSerializer

class TeamViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows gibush to be viewed or edited.
    """
    queryset = Team.objects.all()
    serializer_class = TeamListSerializer

    """
    Description: API endpoint that allows create some gibushim and list al the gibushim
    """
    def create(self, request, *args, **kwargs):
        commander_id = request.data.get('commander')
        team_name = request.data.get('name')
        commander = Commanders.objects.filter(commander_properties_id=commander_id)
        if not commander.exists():
            commander_object = Commanders.objects.create(commander_properties_id=commander_id)
        else:
            commander_object = Commanders.objects.get(commander_properties_id=commander_id)
            #x = Commanders.objects.create(commander_properties=Participent.objects.get(id=))
        file_serializer = TeamCreateSerializer(data={'name':team_name, 'commander': commander_object.id})
        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ReturnParticipentListSpecificGibush(APIView):
    """
    Description: API endpoint that allows filter data from participent table and gibush stats table - can handle with combinations of headers and values
    headers: everything
    """
    def get(self, request, *args, **kwargs):
        participent_fields = Participent._meta.fields
        gibush_stats_fields = GibushStats._meta.fields
        participent_fields_list = [participent.name for participent in participent_fields]
        gibush_stats_fields_list = [stat.name for stat in gibush_stats_fields]
        request_dict_participent = {}
        request_dict_gibushstats = {}
        myun_soldiers = GibushStats.objects.filter(participent_status="נקלט")
        for key, value in request.GET.items():
            if key in participent_fields_list: 
                request_dict_participent[key] = value
            elif key in gibush_stats_fields_list and key != "id":
                request_dict_gibushstats[key] = value
            else:
                if key == "stage":
                    if value == '1':
                        myun_soldiers = GibushStats.objects.filter(first_stage_group=None, participent_status="נקלט").values_list("soldier_id", flat=True)
                        myun_soldiers = GibushStats.objects.all().exclude(soldier_id__in=myun_soldiers)
                    else:
                        myun_soldiers = GibushStats.objects.filter(second_stage_group=None, participent_status="נקלט").values_list("soldier_id", flat=True)
                        myun_soldiers = GibushStats.objects.all().exclude(soldier_id__in=myun_soldiers)
        try:
            myun_soldiers = myun_soldiers.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        except:
            #request_dict_gibushstats["participent_status"] = "נקלט"
            myun_soldiers = GibushStats.objects.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        queryset = Participent.objects.filter(id__in=myun_soldiers, **request_dict_participent)
        file_serializer = ParticipentSerializer(queryset, many=True)
        return Response(file_serializer.data)

class ReturnParticipentListEveryOne(APIView):
    """
    Description: API endpoint that allows filter data from participent table and gibush stats table - can handle with combinations of headers and values
    headers: everything
    """
    def get(self, request, *args, **kwargs):
        participent_fields = Participent._meta.fields
        gibush_stats_fields = GibushStats._meta.fields
        participent_fields_list = [participent.name for participent in participent_fields]
        gibush_stats_fields_list = [stat.name for stat in gibush_stats_fields]
        request_dict_participent = {}
        request_dict_gibushstats = {}
        myun_soldiers = GibushStats.objects.all()
        for key, value in request.GET.items():
            if key in participent_fields_list: 
                request_dict_participent[key] = value
            elif key in gibush_stats_fields_list and key != "id":
                request_dict_gibushstats[key] = value
        try:
            myun_soldiers = myun_soldiers.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        except:
            #request_dict_gibushstats["participent_status"] = "נקלט"
            myun_soldiers = GibushStats.objects.filter(**request_dict_gibushstats).values_list("soldier_id", flat=True)
        queryset = Participent.objects.filter(id__in=myun_soldiers, **request_dict_participent)
        file_serializer = ParticipentSerializer(queryset, many=True)
        return Response(file_serializer.data)


class export_participant(APIView):
    def get(self, request, *args, **kwargs):
        gibush = request.GET.get("gibush")
        contents = os.listdir(settings.MEDIA_ROOT)
        filesdir = []
        for content in contents:
            if len(content.split("-")) == 3 and (int(content.split("-")[0]) - int(datetime.datetime.now().year) <= 3):
                files = os.listdir(os.path.join(settings.MEDIA_ROOT, content))
                print(files)
                filesdir.append([(os.path.join(settings.MEDIA_ROOT, content, item)) for item in files])
        print(filesdir)
        flat_list = [item.replace("\\", "/") for sublist in filesdir for item in sublist]
        print(flat_list)
        queryset = Participent.objects.filter(source_file__in=flat_list)
        assessors = Assessor.objects.all()
        gibush = Gibush.objects.get(pk=gibush)
        assessor_serializer = AssessorSerializer(assessors, many=True)
        participant_serializer = ParticipentExportSerializer(queryset, many=True)
        gibush_serializer = GibushSerializer(gibush)
        return Response({'api.participant':participant_serializer.data, 'api.assessor':assessor_serializer.data, 'api.gibush':gibush_serializer.data}, status=status.HTTP_200_OK)


class ImportGibush(APIView):
    def post(self, request, *args, **kwargs):
        json_file = request.data.get('file')
        data = json.loads(json_file.read().decode())
        for model, model_data in list(data.items()):
            model_obj = getattr(sys.modules[__name__], model)
            print(model_obj)
            for db_instance in model_data:
                if db_instance.get('id'):
                    instance = model_obj.objects.filter(id=db_instance.get('id'))
                    if instance.exists():
                        instance.update(**db_instance)
                    else:
                        print(db_instance, "itay")
                        model_obj.objects.create(**db_instance)
                elif db_instance.get('pk'):
                    instance = model_obj.objects.filter(pk=db_instance.get('pk'))
                    if instance.exists():
                        instance.update(**db_instance)
                    else:
                        model_obj.objects.create(**db_instance)
                else:
                    model_obj.objects.create(**db_instance)
        return Response("success", status=status.HTTP_201_CREATED)


class ReturnAllAssociatedModelsOfParticipant(APIView):
    def get(self, request, *args, **kwargs):
        fixed_dict = []
        participant_id = request.GET.get("id")
        myunim = GibushStats.objects.filter(soldier_id=participant_id).values_list("id", flat=True)
        for myun in myunim:
            fixed_dict.append({'attribute': 'myun_id', 'value': myun})
        teams = RouteSoldier.objects.filter(soldier_id=participant_id).values_list("team_id", flat=True)
        for team in teams:
            fixed_dict.append({'attribute': 'team_id', 'value': team})
        field_days = FieldDaysStats.objects.filter(participant_id=participant_id).values_list("field_day_id", flat=True)
        for field_day in field_days:
            fixed_dict.append({'attribute': 'field_day', 'value': field_day})
        file_serializer = ReturnAllAssociatedModelsOfParticipantSerializer(fixed_dict, many=True)
        return Response(file_serializer.data)
        

class ReturnAllRouteDataOfParticipant(APIView):
    def get(self, request, *args, **kwargs):
        fixed_dict = []
        participant_id = request.GET.get("id")
        team_id = request.GET.get("team_id")
        final_grades = calculate_final_grades_all_stages(team_id, participant_id)
        file_serializer = RouteParticipantProfileSerializer(final_grades)
        return Response(file_serializer.data)
        
    
class RetirementsView(viewsets.ModelViewSet):
    """
    Description: API endpoint that allows retire person and list all retirements
    get = list
        return first name, last name, identity number for all retirements 
    post=create
        scenario: retire participant from gibush
                when - action is not delete
                and - soldier in myun is not exists in retirements table
                then - add soldier in myun to retirements table with those fields: retirements reason, days in gibush, retirement stage.

        scenario: return retire participant to gibush
                when - action is delete
                then - update participant status ib GibushStats table to - "נקלט"
                and - delete soldier in myun from retirements table

        scenario: update retire participant from gibush
                when - action is not delete
                and - soldier in myun exists in retirements table
                then - update soldier in myun object in retirements table with those fields: retirements reason, days in gibush, retirement stage.

    """
    queryset = RouteRetirements.objects.all()
    serializer_class = ListRetirementsSerializer
    def list(self, request, *args, **kwargs):
        team_id = request.GET.get('team_id')
        queryset = RouteRetirements.objects.filter(soldier__team_id=team_id)
        file_serializer = ListRetirementsSerializer(queryset, many=True)
        return Response(file_serializer.data)

    def create(self, request, *args, **kwargs):
        action = request.data.pop("action")
        if action == "delete":
            RouteSoldier.objects.filter(id=request.data["soldier"]).update(status="מסלול")
            RouteRetirements.objects.filter(soldier=request.data["soldier"]).delete()
            logging_conf.logger.debug("participant num {}, has reaccept to route and has deleted from retirements".format(request.data.get("soldier")))
            return Response("success", status=status.HTTP_200_OK)
        else:
            participant_stat = RouteRetirements.objects.filter(soldier_id=request.data["soldier"])
            try:
                RouteSoldier.objects.filter(id=int(request.data["soldier"])).update(status="הודח")
            except:
                pass
            if "soldier_id" in list(request.data.keys()) and participant_stat.exists():
                participent_stat = RouteRetirements.objects.get(soldier=request.data["soldier"])
                file_serializer = CreateRetirementsSerializer(participant_stat, data=request.data, partial=True)
                logging_conf.logger.debug("retirement stats has been updated for participant num {}".format(request.data.get("soldier")))
            else:
                file_serializer = CreateRetirementsSerializer(data=request.data)
                logging_conf.logger.debug("participant num {}, has retire from gibush".format(request.data.get("soldier")))
            if file_serializer.is_valid():
                file_serializer.save()
                return Response("success", status=status.HTTP_201_CREATED)
            else:
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommanderAssessmentView(viewsets.ModelViewSet):
    queryset = CommanderAssessments.objects.all()
    serializer_class = CommanderAssessmentsSerializer
    def list(self, request, *args, **kwargs):
        soldier_id = request.GET.get("soldier")
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        if not sozio_stage == "placeHolder":
            queryset = RouteSoldier.objects.annotate(zil=FilteredRelation('soldier_assessment', condition=Q(soldier_assessment__sozio_stage=sozio_stage))).annotate(comment=(F('zil__comment')), grade=(F('zil__grade')), sozio_stage=(F('zil__sozio_stage'))).filter(team_id=team_id)
        else:
            queryset = RouteSoldier.objects.annotate(zil=FilteredRelation('soldier_assessment')).annotate(comment=(F('zil__comment')), grade=(F('zil__grade')), sozio_stage=(F('zil__sozio_stage'))).filter(team_id=team_id)
        file_serializer = SozioRouteListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    def create(self, request, *args, **kwargs):
        soldier_id = request.data.get("soldier")
        team_id = request.data.pop("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        commander = Team.objects.get(id=team_id)
        request.data['commander'] = commander.commander.id
        assessment = CommanderAssessments.objects.filter(soldier_id=soldier_id, commander_id=commander.commander.id, sozio_stage=sozio_stage)
        if assessment.exists():
            file_serializer = CommanderAssessmentsSerializer(assessment, data=request.data, partial=True)
            logging_conf.logger.debug("commander assessed update successfully")
        else:
            file_serializer = CommanderAssessmentsSerializer(data=request.data)
            logging_conf.logger.debug("commander assessed successfully")
        if file_serializer.is_valid():
            file_serializer.save()
            return Response("success", status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class export_participant(APIView):
    def get(self, request, *args, **kwargs):
        gibush = request.GET.get("gibush")
        contents = os.listdir(settings.MEDIA_ROOT)
        filesdir = []
        for content in contents:
            if len(content.split("-")) == 3 and (int(content.split("-")[0]) - int(datetime.datetime.now().year) <= 3):
                files = os.listdir(os.path.join(settings.MEDIA_ROOT, content))
                filesdir.append([(os.path.join(settings.MEDIA_ROOT, content, item)) for item in files])
        flat_list = [item.replace("\\", "/") for sublist in filesdir for item in sublist]
        queryset = Participent.objects.filter(source_file__in=flat_list)
        assessors = Assessor.objects.all()
        gibush = Gibush.objects.get(pk=gibush)
        assessor_serializer = AssessorSerializer(assessors, many=True)
        participant_serializer = ParticipentExportSerializer(queryset, many=True)
        gibush_serializer = GibushSerializer(gibush)
        return Response({'api.participant':participant_serializer.data, 'api.assessor':assessor_serializer.data, 'api.gibush':gibush_serializer.data}, status=status.HTTP_200_OK)


class ImportGibush(APIView):
    def post(self, request, *args, **kwargs):
        json_file = request.data.get('json_file')
        data = json.loads(json_file.read().decode())
        for model, model_data in list(data.items()):
            model_obj = getattr(sys.modules[__name__], model)
            print(model_obj)
            for db_instance in model_data:
                if db_instance.get('id'):
                    instance = model_obj.objects.filter(id=db_instance.get('id'))
                    if instance.exists():
                        print(db_instance)
                        instance.update(**db_instance)
                    else:
                        print(db_instance, "itay")
                        model_obj.objects.create(**db_instance)
                elif db_instance.get('pk'):
                    instance = model_obj.objects.filter(pk=db_instance.get('pk'))
                    if instance.exists():
                        instance.update(**db_instance)
                    else:
                        model_obj.objects.create(**db_instance)
                else:
                    model_obj.objects.create(**db_instance)
        return Response("success", status=status.HTTP_201_CREATED)
