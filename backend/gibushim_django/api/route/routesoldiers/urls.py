from django.urls import include, path
from rest_framework import routers
from api.route.routesoldiers import views
from gibushim_django import settings
from django.conf.urls.static import static

# default router of  of viewsets - creates endpoints to be viewed or edited.
router = routers.DefaultRouter()
router.register(r'teams', views.TeamViewSet)
router.register(r'route_soldier', views.RouteSoldierViewSet)
router.register(r'team_soldiers', views.TeamSoldiersViewSet)
router.register(r'Retirements', views.RetirementsView)
router.register(r'commander_assessment', views.CommanderAssessmentView)
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('add_members_options/', views.JoinToTeamParticipantOptions.as_view()),
    path('routeSoldier_profile/', views.ReturnAllRouteDataOfParticipant.as_view()),
    path('associated_models/', views.ReturnAllAssociatedModelsOfParticipant.as_view()),
    path('import_gibush/', views.ImportGibush.as_view()),
    path('export_gibush/', views.export_participant.as_view()),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
