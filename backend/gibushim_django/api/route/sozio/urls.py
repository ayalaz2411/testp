from django.urls import include, path
from rest_framework import routers
from api.route.sozio import views
from gibushim_django import settings
from django.conf.urls.static import static

# default router of  of viewsets - creates endpoints to be viewed or edited.
router = routers.DefaultRouter()
router.register(r'SozioRoute', views.SozioRouteGradesViewSet)
router.register(r'TrustWorthy', views.SozioRouteTrustWorthyViewSet)
router.register(r'Freindship', views.SozioRouteFreindshipViewSet)
router.register(r'FitCommander', views.SozioRouteFitCommanderViewSet)
router.register(r'CopingWithStress', views.SozioRouteCopingWithStressViewSet)
router.register(r'NotMatch', views.SozioRouteNotMatchViewSet)
router.register(r'CommanderFeedback', views.SozioRouteCommanderFeedBackViewSet)
router.register(r'FinalResult', views.SozioRouteFinalResultsViewSet)
router.register(r'CommentSummaries', views.SozioRouteSummaries)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('filled_documents/', views.FilledDocuments.as_view()),
    path('comments_summary/', views.CommentSummary.as_view()),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
