from django.contrib.auth.models import User, Group
from api.models import SozioRouteGrades, Participent, Team, RouteSoldier, SozioRouteTrustWorthy, SozioRouteFreindship, SozioRouteFitCommander, SozioRouteCopingWithStress, SozioRouteNotMatch, SozioRouteCommanderFeedBack, RouteRetirements, SozioRouteCommentsSummary
from api.route.routesoldiers.serializers import RouteSoldierSerializer, RouteSoldierParticipentPropertiesSerializer
from api.gibush.serializers import ParticipentSerializer
from rest_framework import serializers
from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator
from django.core.files.base import ContentFile
from gibushim_django import settings
import os
from django.conf.urls.static import static

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class CheckedDocumentsSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    check = serializers.BooleanField()
    
    class Meta:
        ordering = ["id",]


class SozioRouteGradesListSerializer(serializers.HyperlinkedModelSerializer):
    subject_participent_id = RouteSoldierSerializer(read_only=True)
    comment = serializers.CharField(max_length=1000, required=False)
    class Meta:
        model = SozioRouteGrades
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade",
                    "comment"] 


class SozioRouteTrustWorthyListSerializer(serializers.HyperlinkedModelSerializer):
    subject_participent_id = RouteSoldierSerializer(read_only=True)
    comment = serializers.CharField(max_length=1000, required=False)
    class Meta:
        model = SozioRouteTrustWorthy
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade",
                    "comment"] 

class SozioRouteTrustWorthyPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteTrustWorthy
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade",
                    "comment"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteTrustWorthy.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class SozioRouteCopingWithStressPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteCopingWithStress
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade",
                    "comment"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteCopingWithStress.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)



class SozioRouteGradesPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteGrades
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "grade"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteGrades.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)

class SozioRouteFreindshipPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteFreindship
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage"] 

    def create(self, validated_data):
        return SozioRouteFreindship.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class SozioRouteFreindshipListSerializer(serializers.HyperlinkedModelSerializer):
    subject_participent_id = RouteSoldierParticipentPropertiesSerializer(read_only=True)
    document_number = serializers.IntegerField(required=False)
    sozio_stage = serializers.IntegerField(required=False)
    checked = serializers.BooleanField(allow_null=True)
    class Meta:
        model = SozioRouteFreindship
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "checked"] 


class SozioRouteFitCommanderPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteFitCommander
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "comment"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteFitCommander.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class SozioRouteNotMatchPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteNotMatch
        fields =   ["subject_participent_id",
                    "document_number",
                    "sozio_stage",
                    "comment",
                    "rejects"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteNotMatch.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class SozioRouteCommanderFeedbackListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SozioRouteCommanderFeedBack
        fields =   ["document_number",
                    "sozio_stage",
                    "professionality",
                    "friendship",
                    "command",
                    "team_feedback",
                    "team_extra_comment"]


class SozioRouteCommanderFeedbackPostSerializer(serializers.ModelSerializer):
    professionality = serializers.CharField(required=False)
    friendship = serializers.CharField(required=False)
    command = serializers.CharField(required=False)
    team_feedback = serializers.CharField(required=False)
    team_extra_comment = serializers.CharField(required=False)

    class Meta:
        model = SozioRouteCommanderFeedBack
        fields =   ["team",
                    "document_number",
                    "sozio_stage",
                    "professionality",
                    "friendship",
                    "command",
                    "team_feedback",
                    "team_extra_comment"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteCommanderFeedBack.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class SozioRouteListSerializer(serializers.ModelSerializer):
    soldier_name = serializers.SerializerMethodField()
    team_name = serializers.SerializerMethodField()
    sozio_stage = serializers.IntegerField(required=False)
    comment = serializers.CharField(required=False, max_length=1000)
    grade = serializers.IntegerField(required=False)
    checked = serializers.BooleanField(required=False)
    rejects = serializers.CharField(required=False, max_length=1000)
    class Meta:
        model = RouteSoldier
        fields =   ['id',
                    'soldier',
                    'soldier_name',
                    'team',
                    'team_name',
                    'checked',
                    'comment',
                    'grade',
                    'sozio_stage',
                    'rejects']
    
    def get_soldier_name(self, instance):
        if instance.soldier:
            return "{} {}".format(instance.soldier.first_name, instance.soldier.last_name)

    def get_team_name(self, instance):
        if instance.team:
            return instance.team.name


class SozioRouteCommentSummarySerializer(serializers.ModelSerializer):
    subject_participant_properties = serializers.SerializerMethodField()
    subject_value = serializers.SerializerMethodField(read_only=True)
    reject = serializers.CharField(required=False)
    class Meta:
        model = SozioRouteCommentsSummary
        fields =   ["subject_participant",
                    "subject_participant_properties",
                    "sozio_stage",
                    "comment",
                    "reject",
                    "subject",
                    "subject_value"] 

    def get_subject_participant_properties(self, instance):
        return RouteSoldierParticipentPropertiesSerializer(instance.subject_participant).data
    
    def get_subject_value(self, instance):
        return instance.get_value()

    def create(self, validated_data):
        print(validated_data)
        return SozioRouteCommentsSummary.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class SozioRouteAllCommentSerializer(serializers.Serializer):
    comment = serializers.CharField()
    rejects = serializers.CharField(required=False)
    class Meta:
        fields =    ['comment', 'rejects']
    

class SozioRouteTotalGradesSerializer(serializers.Serializer):
    soldier_name = serializers.SerializerMethodField()
    id = serializers.IntegerField()
    user_trustworthy_avg = serializers.IntegerField(allow_null=True)
    user_stress_avg = serializers.IntegerField(allow_null=True)
    total_soziometry = serializers.IntegerField(allow_null=True)
    total_fit_commanders = serializers.IntegerField(allow_null=True)
    total_not_match = serializers.IntegerField(allow_null=True)
    total_friendship = serializers.IntegerField(allow_null=True)
    commander_evaluation = serializers.IntegerField(allow_null=True)
    class Meta:
        fields =    ['id',
                    'commander_evaluation',
                    'soldier_name',
                    'user_stress_avg',
                    'user_trustworthy_avg',
                    'total_fit_commanders',
                    'total_soziometry',
                    'total_not_match',
                    'total_friendship']
    
    def get_soldier_name(self, instance):
        x =  Participent.objects.get(id=instance.get("soldier"))
        if x:
            print(x)
            return "{} {}".format(x.first_name, x.last_name)


class SozioRouteFinalGradesSerializer(serializers.Serializer):
    soldier_values = SozioRouteTotalGradesSerializer(many=True)
    class Meta:
        fields =   ['soldier_values', 'team']

