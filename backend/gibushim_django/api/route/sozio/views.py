import json
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics, status
from django.db.models import F, Q, FilteredRelation,  BooleanField, Case, When, Value, IntegerField, Count, Sum, Avg, FloatField, Window
from django.db.models.functions import DenseRank
from itertools import chain
from django.conf import settings
from django.db.utils import DataError
from api.route.routesoldiers.serializers import RouteSoldierSerializer, RouteSoldierParticipentPropertiesSerializer
from api.route.sozio.serializers import SozioRouteGradesPostSerializer, SozioRouteGradesListSerializer, CheckedDocumentsSerializer, SozioRouteTrustWorthyPostSerializer, SozioRouteTrustWorthyListSerializer, SozioRouteFreindshipListSerializer, SozioRouteFreindshipPostSerializer, SozioRouteFitCommanderPostSerializer, SozioRouteListSerializer, SozioRouteCopingWithStressPostSerializer, SozioRouteNotMatchPostSerializer, SozioRouteCommanderFeedbackPostSerializer, SozioRouteCommanderFeedbackListSerializer, SozioRouteFinalGradesSerializer, SozioRouteAllCommentSerializer, SozioRouteCommentSummarySerializer
from api.models import SozioRouteGrades, Team, RouteSoldier, SozioRouteTrustWorthy, SozioRouteFreindship, SozioRouteFitCommander, SozioRouteCopingWithStress, SozioRouteNotMatch, SozioRouteCommanderFeedBack, RouteRetirements, SozioRouteCommentsSummary, CommanderAssessments
from rest_framework.views import APIView
from rest_framework.response import Response
import csv
import os
from django.conf import settings
from io import BytesIO
from django.http import HttpResponse
from api.assets import logging_conf
import sys

MEDIA_ROOT = settings.MEDIA_ROOT

ASSESSMENT_FIELDS_MODELS = [SozioRouteTrustWorthy, SozioRouteFitCommander, SozioRouteNotMatch, SozioRouteCopingWithStress]


class FilledDocuments(APIView):
    def get(self, request, *args, **kwargs):
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        route_soldiers = RouteSoldier.objects.filter(~Q(status="הודח"), team_id=team_id)
        route_filled_documents = list(route_soldiers.filter(grades_filled__sozio_stage=sozio_stage).values_list('grades_filled__document_number', flat=True))
        dict_route_soldiers = list(route_soldiers.values())
        for i in range(1, len(dict_route_soldiers) + 1):
            if i in route_filled_documents:
                dict_route_soldiers[i-1]["check"] = True
            else:
                dict_route_soldiers[i-1]["check"] = False
        file_serializer = CheckedDocumentsSerializer(dict_route_soldiers, many=True)
        return Response(file_serializer.data)

class CommentSummary(APIView):
    def get(self, request, *args, **kwargs):
        soldier_id = request.GET.get("soldier_id")
        assessment_field = int(request.GET.get("assessment_field"))
        sozio_stage = request.GET.get("sozio_stage")
        comments_summary = ASSESSMENT_FIELDS_MODELS[assessment_field - 1].objects.filter(subject_participent_id_id=soldier_id, sozio_stage=sozio_stage, comment__isnull=False)
        file_serializer = SozioRouteAllCommentSerializer(comments_summary, many=True)
        return Response(file_serializer.data)



class SozioRouteSummaries(viewsets.ModelViewSet): 
    queryset = SozioRouteTrustWorthy.objects.all()
    serializer_class = SozioRouteListSerializer
    def list(self, request, *args, **kwargs):
        soldier_id = request.GET.get("soldier_id")
        team_id = request.GET.get("team_id")
        subject = request.GET.get("subject")
        sozio_stage = request.GET.get("sozio_stage")
        if team_id:
            queryset = SozioRouteCommentsSummary.objects.filter(~Q(subject_participant__status='הודח'), subject_participant__team_id=team_id, sozio_stage=sozio_stage).order_by('subject', 'subject_participant')
            file_serializer = SozioRouteCommentSummarySerializer(queryset, many=True)
        elif soldier_id:
            queryset = SozioRouteCommentsSummary.objects.filter(~Q(subject_participant__status='הודח'), subject_participant_id=soldier_id, sozio_stage=sozio_stage, subject=subject)
            file_serializer = SozioRouteCommentSummarySerializer(queryset)
        return Response(file_serializer.data)

    def create(self, request, *args, **kwargs):
        print(request.data)
        comment = request.data.get("comment")
        if comment:
            del request.data["comment"]
        reject = request.data.get("reject")
        if reject:
            del request.data["reject"]
        queryset = SozioRouteCommentsSummary.objects.filter(**request.data)
        if (queryset.exists()):
            if reject:
                file_serializer = SozioRouteCommentSummarySerializer(queryset, data={**request.data, 'reject':reject}, partial=True)
            elif comment:
                file_serializer = SozioRouteCommentSummarySerializer(queryset, data={**request.data, 'comment':comment}, partial=True)
        else:
            file_serializer = SozioRouteCommentSummarySerializer(data={**request.data, 'comment':comment, 'reject':reject})
        if file_serializer.is_valid():
            file_serializer.save()
            return Response("success", status=status.HTTP_200_OK)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SozioRouteTrustWorthyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteTrustWorthy.objects.all()
    serializer_class = SozioRouteListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        queryset = RouteSoldier.objects.annotate(zil=FilteredRelation('trustworthy_filled', condition=Q(trustworthy_filled__document_number=document_number) & Q(trustworthy_filled__sozio_stage=sozio_stage))).annotate(comment=(F('zil__comment')), grade=(F('zil__grade'))).filter(~Q(status='הודח'), team_id=team_id)
        file_serializer = SozioRouteListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):

        keys_for_update = ["document_number", "subject_participent_id", "sozio_stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioRouteTrustWorthy.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioRouteTrustWorthyPostSerializer(grades, data=sozio_dict, partial=True)
            else:
                file_serializer = SozioRouteTrustWorthyPostSerializer(data=sozio_dict)
            if file_serializer.is_valid():
                file_serializer.save()
                """
                self.subject_id = sozio_dict["subject_participent_id"]
                self.sozio_type = sozio_dict["sozio_stage"]
                self.by_subject = SozioRouteGrades.objects.filter(subject_participent_id=self.subject_id, sozio_stage=self.sozio_type)
                self.was_subject = self.by_subject.count()
                self.RouteSoldier = RouteStats.objects.filter(soldier_id=self.subject_id)
                self.group = self.RouteSoldier.values("team_id")
                self.people_in_group = RouteStats.objects.filter(team_id=self.group[0]["team_id"]).count() - 1
                result = self.calculate_sozio()
                """
            else:
                print(file_serializer.errors)
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)


class SozioRouteCommanderFeedBackViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteCommanderFeedBack.objects.all()
    serializer_class = SozioRouteCommanderFeedbackListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        queryset = Team.objects.annotate(zil=FilteredRelation('commander_feedback', condition=Q(commander_feedback__document_number=document_number) & Q(commander_feedback__sozio_stage=sozio_stage))).annotate(professionality=(F('zil__professionality')), friendship=(F('zil__friendship')), command=(F('zil__command')), team_feedback=(F('zil__team_feedback')), team_extra_comment=(F('zil__team_extra_comment'))).filter(id=team_id)
        file_serializer = SozioRouteCommanderFeedbackListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):
        keys_for_update = ["document_number", "team", "sozio_stage"]
        data_for_update = {}
        for key in keys_for_update:
            data_for_update[key] = request.data[key]
        grades = SozioRouteCommanderFeedBack.objects.filter(**data_for_update)
        if all(elem in list(request.data.keys())  for elem in keys_for_update) and grades.exists():
            file_serializer = SozioRouteCommanderFeedbackPostSerializer(grades, data=request.data, partial=True)
        else:
            file_serializer = SozioRouteCommanderFeedbackPostSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
        else:
            print(file_serializer.errors)
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)


class SozioRouteFitCommanderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteFitCommander.objects.all()
    serializer_class = SozioRouteListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        queryset = RouteSoldier.objects.annotate(zil=FilteredRelation('fit_commander_filled', condition=Q(fit_commander_filled__document_number=document_number) & Q(fit_commander_filled__sozio_stage=sozio_stage))).annotate(checked=(F('zil')), comment=(F('zil__comment'))).filter(~Q(status='הודח'), team_id=team_id)
        file_serializer = SozioRouteListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):
        document_number = request.data[0].get('document_number')
        sozio_stage = request.data[0].get('sozio_stage')
        commanders = SozioRouteFitCommander.objects.filter(document_number=document_number, sozio_stage=sozio_stage)
        if commanders.count() > 0:
            commanders.delete()
        keys_for_update = ["document_number", "subject_participent_id", "sozio_stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioRouteFitCommander.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioRouteFitCommanderPostSerializer(grades, data=sozio_dict, partial=True)
            else:
                file_serializer = SozioRouteFitCommanderPostSerializer(data=sozio_dict)
            if file_serializer.is_valid():
                file_serializer.save()
            else:
                print(file_serializer.errors)
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)


class SozioRouteFinalResultsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteNotMatch.objects.all()
    serializer_class = SozioRouteFinalGradesSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        team_soldiers = RouteSoldier.objects.filter(~Q(status='הודח'), team_id=team_id)
        all_team_friends = team_soldiers.aggregate(choosed=Count(Case(
        When(Q(freindship_filled__sozio_stage=sozio_stage), then=1),
        output_field=IntegerField())))
        all_team_fit_commander = team_soldiers.aggregate(choosed=Count(Case(
        When(Q(fit_commander_filled__sozio_stage=sozio_stage), then=1),
        output_field=IntegerField())))
        all_team_not_match = team_soldiers.aggregate(choosed=Count(Case(
        When(Q(not_match_filled__sozio_stage=sozio_stage), then=1),
        output_field=IntegerField())))

        not_match = (team_soldiers.annotate(count_user_not_match=Count(Case(
        When(Q(not_match_filled__sozio_stage=sozio_stage), then=1),
        output_field=IntegerField(),
        ))).annotate(total_not_match=F('count_user_not_match')  * 100 / all_team_not_match.get('choosed')).values('total_not_match'))

        friendship_results = (team_soldiers.annotate(count_friendly_user=Count(Case(
        When(Q(freindship_filled__sozio_stage=sozio_stage), then=1),
        output_field=IntegerField(),
        ))).annotate(total_friendship=F('count_friendly_user')  * 100 / all_team_friends.get('choosed')).values('total_friendship'))

        fit_commander_results = (team_soldiers.annotate(count_user_fit_commander=Count(Case(
        When(Q(fit_commander_filled__sozio_stage=sozio_stage), then=1),
        output_field=IntegerField(),
        ))).annotate(total_fit_commanders=F('count_user_fit_commander')  * 100 / all_team_fit_commander.get('choosed')).values('total_fit_commanders'))

        sozio_results = (team_soldiers.annotate(count_user_sozio=Count(Case(
        When(Q(grades_filled__sozio_stage=sozio_stage), then=1),
        output_field=IntegerField(),
        ))).annotate(sum_user_sozio=Sum(Case(
        When(Q(grades_filled__sozio_stage=sozio_stage), then=F('grades_filled__grade')),
        output_field=IntegerField())))
        .annotate(total_soziometry= 100 - ((100 / (((F('count_user_sozio')) ** 2) - (F('count_user_sozio')))) * (F('sum_user_sozio') - (F('count_user_sozio'))))).values('total_soziometry'))

        trustworthy_results = (team_soldiers.annotate(user_trustworthy_avg=Avg(Case(
        When(Q(trustworthy_filled__sozio_stage=sozio_stage), then=F('trustworthy_filled__grade')),
        output_field=FloatField(),
        ))).values('user_trustworthy_avg'))

        commander_evaluations = team_soldiers.annotate(zil=FilteredRelation('soldier_assessment', condition=Q(soldier_assessment__sozio_stage=sozio_stage))).annotate(commander_evaluation=F('zil__grade')).values('commander_evaluation')

        stress_results = team_soldiers.annotate(user_stress_avg=Avg(Case(
        When(Q(stress_filled__sozio_stage=sozio_stage), then=F('stress_filled__grade')),
        output_field=FloatField(),
        ))).values('user_stress_avg', 'id', 'soldier')
        combine_querysets = []
        try:
            for i in range(len(team_soldiers)):
                combine_querysets.append({**commander_evaluations[i], **stress_results[i], **trustworthy_results[i], **sozio_results[i], **fit_commander_results[i], **not_match[i], **friendship_results[i]})
        except DataError:
            return Response([])
        serializer_data = {'soldier_values': combine_querysets, 'team':team_id}
        file_serializer = SozioRouteFinalGradesSerializer(serializer_data)
        return Response(file_serializer.data)


class SozioRouteNotMatchViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteNotMatch.objects.all()
    serializer_class = SozioRouteListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        queryset = RouteSoldier.objects.annotate(zil=FilteredRelation('not_match_filled', condition=Q(not_match_filled__document_number=document_number) & Q(not_match_filled__sozio_stage=sozio_stage))).annotate(rejects=(F('zil__rejects')), comment=(F('zil__comment'))).filter(~Q(status='הודח'), team_id=team_id)
        file_serializer = SozioRouteListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):
        document_number = request.data[0].get('document_number')
        sozio_stage = request.data[0].get('sozio_stage')
        not_match = SozioRouteNotMatch.objects.filter(document_number=document_number, sozio_stage=sozio_stage)
        if not_match.count() > 0:
            not_match.delete()
        keys_for_update = ["document_number", "subject_participent_id", "sozio_stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioRouteNotMatch.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioRouteNotMatchPostSerializer(grades, data=sozio_dict, partial=True)
            else:
                file_serializer = SozioRouteNotMatchPostSerializer(data=sozio_dict)
            if file_serializer.is_valid():
                file_serializer.save()
            else:
                print(file_serializer.errors)
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)


class SozioRouteCopingWithStressViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteCopingWithStress.objects.all()
    serializer_class = SozioRouteListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.
        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        queryset = RouteSoldier.objects.annotate(zil=FilteredRelation('stress_filled', condition=Q(stress_filled__document_number=document_number) & Q(stress_filled__sozio_stage=sozio_stage))).annotate(comment=(F('zil__comment')), grade=(F('zil__grade'))).filter(~Q(status='הודח'), team_id=team_id)
        file_serializer = SozioRouteListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):

        keys_for_update = ["document_number", "subject_participent_id", "sozio_stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioRouteCopingWithStress.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioRouteCopingWithStressPostSerializer(grades, data=sozio_dict, partial=True)
            else:
                file_serializer = SozioRouteCopingWithStressPostSerializer(data=sozio_dict)
            if file_serializer.is_valid():
                file_serializer.save()
            else:
                print(file_serializer.errors)
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)


class SozioRouteGradesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteGrades.objects.all()
    serializer_class = SozioRouteListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.

        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        queryset = RouteSoldier.objects.annotate(zil=FilteredRelation('grades_filled', condition=Q(grades_filled__document_number=document_number) & Q(grades_filled__sozio_stage=sozio_stage))).annotate(grade=(F('zil__grade'))).filter(~Q(status='הודח'), team_id=team_id)
        file_serializer = SozioRouteListSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):

        keys_for_update = ["document_number", "subject_participent_id", "sozio_stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioRouteGrades.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioRouteGradesPostSerializer(grades, data=sozio_dict, partial=True)
                print(1)
            else:
                file_serializer = SozioRouteGradesPostSerializer(data=sozio_dict)
                print(2)
            if file_serializer.is_valid():
                file_serializer.save()
            else:
                print(file_serializer.errors)
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)

    # r=100-(100/((p-1)^{2}-p))*(SUM-(p-1))))
    def calculate_sozio(self):
        if self.was_subject == self.people_in_group:
            self.grades = self.by_subject.values_list("grade", flat=True)
            p = self.was_subject
            result = 100 - ((100 / ((p ** 2) - p)) * (sum(self.grades) - p))
            return float(result)
        else:
            return None


class SozioRouteFreindshipViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows participent to be viewed or edited.
    """
    queryset = SozioRouteFreindship.objects.all()
    serializer_class = SozioRouteFreindshipListSerializer
    def list(self, request, *args, **kwargs):
        """
        filter the assessoringroup db by headers in url.

        headers: field=<name of header>, group_id=<value>, stage=<int> 
        example: field=group_id, group_id=3
        """
        team_id = request.GET.get("team_id")
        sozio_stage = request.GET.get("sozio_stage")
        document_number = request.GET.get("document_number")
        queryset = RouteSoldier.objects.filter(~Q(status='הודח'), team_id=team_id).annotate(zil=FilteredRelation('freindship_filled', condition=Q(freindship_filled__document_number=document_number) & Q(freindship_filled__sozio_stage=sozio_stage))).annotate(checked=(F('zil')))
        file_serializer = RouteSoldierParticipentPropertiesSerializer(queryset, many=True)
        return Response(file_serializer.data)

    
    def create(self, request, *args, **kwargs):
        document_number = request.data[0].get('document_number')
        sozio_stage = request.data[0].get('sozio_stage')
        friends = SozioRouteFreindship.objects.filter(document_number=document_number, sozio_stage=sozio_stage)
        if friends.count() > 0:
            friends.delete()
        keys_for_update = ["document_number", "subject_participent_id", "sozio_stage"]
        for sozio_dict in request.data:
            data_for_update = {}
            for key in keys_for_update:
                data_for_update[key] = sozio_dict[key]
            grades = SozioRouteFreindship.objects.filter(**data_for_update)
            if all(elem in list(sozio_dict.keys())  for elem in keys_for_update) and grades.exists():
                file_serializer = SozioRouteFreindshipPostSerializer(grades, data=sozio_dict, partial=True)
                print(1)
            else:
                file_serializer = SozioRouteFreindshipPostSerializer(data=sozio_dict)
                print(2)
            if file_serializer.is_valid():
                file_serializer.save()
            else:
                print(file_serializer.errors)
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response("success", status=status.HTTP_201_CREATED)

    # r=100-(100/((p-1)^{2}-p))*(SUM-(p-1))))
    def calculate_sozio(self):
        if self.was_subject == self.people_in_group:
            self.grades = self.by_subject.values_list("grade", flat=True)
            p = self.was_subject
            result = 100 - ((100 / ((p ** 2) - p)) * (sum(self.grades) - p))
            return float(result)
        else:
            return None


def calculate_final_grades_all_stages(team_id, soldier_id):
        team_soldiers = RouteSoldier.objects.filter(~Q(status='הודח'), team_id=team_id)
        soldier = RouteSoldier.objects.filter(team_id=team_id, soldier_id=soldier_id)
        all_team_friends = team_soldiers.aggregate(choosed=Count(F('freindship_filled')))
        all_team_fit_commander = team_soldiers.aggregate(choosed=Count(F('fit_commander_filled')))
        all_team_not_match = team_soldiers.aggregate(choosed=Count(F('not_match_filled')))

        not_match = (team_soldiers.annotate(count_user_not_match=Count(F('not_match_filled')))
        .annotate(total_not_match=F('count_user_not_match')  * 100 / all_team_not_match.get('choosed'))
        .annotate(not_match_place=Window(expression=DenseRank(),order_by=[F('total_not_match').asc(),])).values('total_not_match', 'not_match_place'))
        
        friendship_results = (team_soldiers.annotate(count_friendly_user=Count(F('freindship_filled')))
        .annotate(total_friendship=F('count_friendly_user')  * 100 / all_team_friends.get('choosed'))
        .annotate(friendship_place=Window(expression=DenseRank(),order_by=[F('total_friendship').desc(),])).values('total_friendship', 'friendship_place'))

        fit_commander_results = (team_soldiers.annotate(count_user_fit_commander=Count(F('fit_commander_filled')))
        .annotate(total_fit_commander=F('count_user_fit_commander')  * 100 / all_team_fit_commander.get('choosed'))
        .annotate(fit_commander_place=Window(expression=DenseRank(),order_by=[F('total_fit_commander').desc(),]))
        .values('total_fit_commander', 'fit_commander_place'))

        sozio_results = (team_soldiers.annotate(count_user_sozio=Count(F('grades_filled')))
        .annotate(sum_user_sozio=Sum(F('grades_filled__grade')))
        .annotate(total_soziometry= 100 - ((100 / (((F('count_user_sozio')) ** 2) - (F('count_user_sozio')))) * (F('sum_user_sozio') - (F('count_user_sozio')))))
        .annotate(soziometry_place=Window(expression=DenseRank(),order_by=[F('total_soziometry').desc(),]))
        .values('total_soziometry', 'soziometry_place'))
        
        trustworthy_results = (team_soldiers.annotate(user_trustworthy_avg=Avg(F('trustworthy_filled__grade')))
        .annotate(trustworthy_place=Window(expression=DenseRank(),order_by=[F('user_trustworthy_avg').desc(),]))
        .values('user_trustworthy_avg', 'trustworthy_place'))

        stress_results = (team_soldiers.annotate(user_stress_avg=Avg(F('stress_filled__grade')))
        .annotate(stress_place=Window(expression=DenseRank(),order_by=[F('user_stress_avg').desc(),]))
        .values('user_stress_avg', 'id', 'soldier', 'team', 'stress_place'))
        result = {}
        for i in range(len(team_soldiers)):
            if int(stress_results[i].get("soldier")) == int(soldier_id):
                try:
                    return {**stress_results[i], **trustworthy_results[i], **sozio_results[i], **fit_commander_results[i], **not_match[i], **friendship_results[i]}
                except DataError:
                    return {**stress_results[i], **trustworthy_results[i], **sozio_results[i], **fit_commander_results[i], **friendship_results[i]}
                   