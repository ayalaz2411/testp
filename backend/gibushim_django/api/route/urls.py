from django.urls import include, path
from rest_framework import routers
from api.route import views
from gibushim_django import settings
from django.conf.urls.static import static

# default router of  of viewsets - creates endpoints to be viewed or edited.
"""

router.register(r'SozioRoute', views.SozioRouteGradesViewSet)
router.register(r'TrustWorthy', views.SozioRouteTrustWorthyViewSet)
router.register(r'teams', views.TeamViewSet)
router.register(r'route_soldier', views.RouteSoldierViewSet)
router.register(r'team_soldiers', views.TeamSoldiersViewSet)
"""
router = routers.DefaultRouter()

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    path('sozio/', include('api.route.sozio.urls')),
    path('soldiers/', include('api.route.routesoldiers.urls')),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


"""
    path('filled_documents/', views.FilledDocuments.as_view()),
    path('import_gibush/', views.ImportGibush.as_view()),
    path('export_gibush/', views.export_participant.as_view()),
"""