from django.contrib.auth.models import User, Group
from api.models import Gibush, Participent, CsvFile, GibushStats, GibushGroup, Assessor, AssessorInGroup, AssessorEvaluation, SozioGibushGrades, retirements
from rest_framework import serializers
from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator
from django.core.files.base import ContentFile
import base64
import six
from django.conf.urls.static import static
import uuid
from gibushim_django import settings
import os
import imghdr


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class GibushSerializer(serializers.HyperlinkedModelSerializer):
    pk = serializers.IntegerField(required=False)
    class Meta:
        model = Gibush
        fields = ['pk','start_date', 'end_date', 'title', 'description']

    def create(self, validated_data, *args):
        try:
            gibush_id = validated_data.pop("pk")
            validated_data["id"] = gibush_id
            if gibush_id and Gibush.objects.filter(id=gibush_id).exists():
                return self.update(Gibush.objects.filter(id=gibush_id), validated_data) 
            else:
                return Gibush.objects.create(**validated_data)
        except KeyError: 
            return Gibush.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class MarkArrivedParticipentSerializer(serializers.HyperlinkedModelSerializer):
    arrived = serializers.BooleanField(required=False, allow_null=True)
    class Meta:
        model = Participent
        fields = ['id',
                    'first_name',
                    'last_name',
                    'identity_number',
                    'first_phone',
                    'address',
                    'arrived']


class Base64ImageField(serializers.CharField):

    def to_internal_value(self, data):
        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
                
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )
            complete_path = os.path.join(settings.MEDIA_ROOT, complete_file_name)
            
            with open(complete_path, "wb") as f:
                f.write(decoded_file)
        return os.path.join(settings.MEDIA_URL, complete_file_name)

    def get_file_extension(self, file_name, decoded_file):
        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension
        return extension

    def to_representation(self, value):
        return value


class ParticipentSerializer(serializers.HyperlinkedModelSerializer):
    arrived = serializers.BooleanField(required=False, allow_null=True)
    pic = Base64ImageField(
        max_length=None, allow_null=True, required=False
    )
    update = serializers.BooleanField(required=False, allow_null=True)
    id = serializers.IntegerField(required=False)
    class Meta:
        model = Participent
        fields =   ['id',
                    'first_name',
                    'last_name',
                    'identity_number',
                    'personal_number',
                    'birth_date',
                    'first_phone',
                    'second_phone',
                    'third_phone',
                    'address',
                    'is_new_immigrant',
                    'enlistment_status',
                    'kabha',
                    'dapar',
                    'tzadach',
                    'medical_profile',
                    'disqualifying_medical_trait',
                    'source_file',
                    'pic',
                    'arrived'] 

    def create(self, validated_data, *args):
        participant_id = validated_data.get("id")
        identity_number = validated_data.get("identity_number")
        if participant_id and Participent.objects.filter(id=participant_id).exists():
            return self.update(Participent.objects.filter(id=participant_id), validated_data) 
        elif identity_number and Participent.objects.filter(identity_number=identity_number).exists():
            return self.update(Participent.objects.filter(identity_number=identity_number), validated_data) 
        else:
            return Participent.objects.create(validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class AssessorSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = Assessor
        fields =   ['id',
                    'first_name',
                    'last_name',
                    'identity_number',
                    'unit'] 

    def create(self, validated_data, *args):
        assessor_id = validated_data.get("id")
        identity_number = validated_data.get("identity_number")
        if assessor_id and Assessor.objects.filter(id=assessor_id).exists():
            return self.update(Assessor.objects.filter(id=assessor_id), validated_data) 
        elif identity_number and Assessor.objects.filter(identity_number=identity_number).exists():
            return self.update(Assessor.objects.filter(identity_number=identity_number), validated_data) 
        else:
            return Assessor.objects.create(validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class AssessorEvaluationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AssessorEvaluation
        fields = ["myun_id",
                    "soldier_id",
                    "assessor_id",
                    "grade",
                    "stage",
                    "comment"] 
                     
    def create(self, validated_data, *args):
        return AssessorEvaluation.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class AssessorInGroupSerializer(serializers.HyperlinkedModelSerializer):
    role = serializers.CharField(required=False, allow_null=True)
    group_id = serializers.IntegerField(required=False, allow_null=True)
    class Meta:
        model = AssessorInGroup
        fields = ["myun_id",
                    "stage",
                    "group_id",
                    "assessor_id",
                    "role"] 

    def create(self, validated_data):
        print(validated_data)
        return AssessorInGroup.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class SozioGibushGradesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SozioGibushGrades
        fields = ["filler_participent_id",
                    "subject_participent_id",
                    "stage",
                    "grade"] 

    def create(self, validated_data):
        print(validated_data)
        return SozioGibushGrades.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class CsvFileSerializer(serializers.HyperlinkedModelSerializer):
    """
    serializer of filename and date for save file in model
    """
    class Meta:
        model = CsvFile
        fields = ["date_file", "csv_file"]


class FileParseSerializer(serializers.Serializer):
    """
    serializer of filename and pk for open file
    """
    csv_filename = serializers.CharField()
    csv_pk = serializers.CharField()


class GibushStatsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GibushStats
        fields = ['myun_id',
                    'soldier_id',
                    'first_stage_group',
                    'second_stage_group',
                    'first_stage_sozio',
                    'second_stage_sozio',
                    'final_grade',
                    'final_rating',
                    'first_stage_grade',
                    'second_stage_grade',
                    'comment',
                    'passed_gibush',
                    'participent_status']

    def create(self, validated_data):
        return GibushStats.objects.create(**validated_data)

    def update(self, instance, validated_data):
        print(validated_data)
        return instance.update(**validated_data)


class CreateGroupSerializer(serializers.Serializer):
    num_of_groups = serializers.IntegerField()
    stage = serializers.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(2)])
    myun_id = serializers.IntegerField()


class GetGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GibushGroup
        fields = ['group_id', 'stage', 'myun_id']

    
class GroupsAssessorsSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    identity_number = serializers.CharField(max_length=100)
    role = serializers.CharField(max_length=100)
    unit = serializers.CharField(max_length=100)
    assessor_id = serializers.CharField(max_length=9)
    group_id = serializers.IntegerField()
    stage = serializers.IntegerField()
    people_in_group = serializers.IntegerField(default=0)
    


class AssessorNameAndGradesSerializer(serializers.Serializer):
    full_name = serializers.CharField(max_length=100, required=False, allow_null=True)
    comment = serializers.CharField(max_length=100, required=False, allow_null=True)
    stage = serializers.IntegerField(required=False, allow_null=True)
    grade = serializers.IntegerField(required=False, allow_null=True)


class ParticipentProfileSerializer(serializers.ModelSerializer):
    participent = ParticipentSerializer(Participent.objects.all())
    assessors_grade_names = AssessorNameAndGradesSerializer(many=True)
    myun_id = serializers.IntegerField(required=False, allow_null=True)

    class Meta:
        model = GibushStats
        
        fields = ['id',
                    'myun_id',
                    'first_stage_group',
                    'second_stage_group',
                    'first_stage_sozio',
                    'second_stage_sozio',
                    'final_grade',
                    'final_rating',
                    'first_stage_grade',
                    'second_stage_grade',
                    'comment',
                    'participent_status',
                    'passed_gibush',
                    'participent',
                    'assessors_grade_names'] 


class CreateRetirementsSerializer(serializers.ModelSerializer):

    class Meta:
        model = retirements
        fields = ['myun_id',
                'soldier_id',
                'retirements_reason',
                'days_in_gibush',
                'retirement_date',
                'retirement_stage']

class ListRetirementsSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    identity_number = serializers.CharField(max_length=100)

    class Meta:
        model = retirements
        fields=['first_name',
                'last_name',
                'identity_number',
                'soldier_id',
                'retirements_reason',
                'days_in_gibush',
                'retirement_date',
                'retirement_stage']