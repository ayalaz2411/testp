from django.apps import AppConfig
import logging
import logging.config
import shutil
class ApiConfig(AppConfig):
    name = 'api'



logging.config.fileConfig(fname='file.conf', disable_existing_loggers=False)
# Get the logger specified in the file
LOGGER = logging.getLogger(__name__)
