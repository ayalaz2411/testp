# Generated by Django 2.2.11 on 2020-07-06 06:41

import api.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_participent_pic'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sozioroutegrades',
            old_name='filler_participent_id',
            new_name='subject_participent_id',
        ),
        migrations.RemoveField(
            model_name='sozioroutegrades',
            name='sozio_final_grade',
        ),
        migrations.AddField(
            model_name='gibushstats',
            name='passed_gibush',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='sozioroutegrades',
            name='document_number',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='sozioroutegrades',
            name='sozio_stage',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='csvfile',
            name='csv_file',
            field=models.FileField(max_length=1000000, upload_to=api.models.user_directory_path),
        ),
        migrations.AlterField(
            model_name='sozioroutegrades',
            name='grade',
            field=models.IntegerField(null=True),
        ),
        migrations.DeleteModel(
            name='SozioRouteFinalGrades',
        ),
    ]
