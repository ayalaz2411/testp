# Generated by Django 3.0.2 on 2020-08-16 14:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_sozioroutefitcommander'),
    ]

    operations = [
        migrations.CreateModel(
            name='SozioRouteCopingWithStress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sozio_stage', models.IntegerField(null=True)),
                ('document_number', models.IntegerField(null=True)),
                ('grade', models.IntegerField(null=True)),
                ('comment', models.CharField(blank=True, max_length=1000, null=True)),
                ('subject_participent_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='stress_filled', to='api.RouteSoldier')),
            ],
        ),
    ]
