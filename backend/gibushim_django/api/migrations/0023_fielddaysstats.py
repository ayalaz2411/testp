# Generated by Django 2.2.11 on 2020-09-16 13:07

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0022_fieldday'),
    ]

    operations = [
        migrations.CreateModel(
            name='FieldDaysStats',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fit_matkal', models.BooleanField()),
                ('fit_hovlim', models.BooleanField()),
                ('fit_shayetet', models.BooleanField()),
                ('fit_tzolelot', models.BooleanField()),
                ('field_day_num', models.IntegerField()),
                ('group_number', models.IntegerField()),
                ('first_assessor', models.CharField(max_length=100)),
                ('second_assessor', models.CharField(max_length=100)),
                ('first_assessor_grade', models.IntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(6)])),
                ('second_assessor_grade', models.IntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(6)])),
                ('baror_grade', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
                ('sozio_grade', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
                ('praiority_matkal', models.IntegerField()),
                ('praiority_hovlim', models.IntegerField()),
                ('praiority_shayetet', models.IntegerField()),
                ('retirement', models.BooleanField()),
                ('retirement_reason', models.CharField(max_length=100)),
                ('field_day', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='field_day_all_stats', to='api.FieldDay')),
                ('participant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='field_day_participant_stats', to='api.Participent')),
            ],
        ),
    ]
