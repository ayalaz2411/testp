# Generated by Django 2.2.11 on 2020-04-19 14:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_auto_20200419_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='routesoldier',
            name='soldier',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='routers', to='api.Participent'),
        ),
        migrations.AlterField(
            model_name='routesoldier',
            name='team',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='team_soldiers', to='api.Team'),
        ),
    ]
