import json
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics, status
from django.db.models import F, Q, FilteredRelation,  BooleanField, Case, When, Value, IntegerField
from django.conf import settings
from api.fielddays.serializers import FieldDaysSerializer, CsvFileSerializer, FileParseSerializer, FieldDaysStatsProfileSerializer, FieldDaysStatsListSerializer
from api.models import FieldDay, CsvFile, CsvParseError, Participent, FieldDaysStats
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser,JSONParser, BaseParser
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
import csv
import os
import datetime
from django.conf import settings
from collections import defaultdict 
from io import BytesIO
from xhtml2pdf import pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from zipfile import ZipFile
from django.db import connection
from api.assets import logging_conf, csv_output
import sys
import shutil

MEDIA_ROOT = settings.MEDIA_ROOT
TABLE_FIELD_DAYS_FILE_HEADERS = {"מתאים מטכל": "fit_matkal",
                                "מתאים חובלים": "fit_hovlim",
                                "מתאים שייטת": "fit_shayetet",
                                "מתאים צוללות": "fit_tzolelot",
                                "מספר יוש": "field_day_num",
                                "מספר קבוצה": "group_number",
                                "מעריך 1": "first_assessor",
                                "מעריך 2": "second_assessor",
                                "ציון מעריך 1": "first_assessor_grade",
                                "ציון מעריך 2": "second_assessor_grade",
                                "ציון בראור": "baror_grade",
                                "סוציומטרי": "sozio_grade",
                                "עדיפות מטכל": "praiority_matkal",
                                "עדיפות חובלים": "praiority_hovlim",
                                "עדיפות שייטת": "praiority_shayetet",
                                "סיבת הדחה": "retirement_reason"}

TABLE_FILE_HEADERS = {'שם פרטי': 'first_name',
                      'שם משפחה': 'last_name',
                      'תז': 'identity_number',  
                      'מספר אישי': 'personal_number',
                      'תאריך לידה': 'birth_date',
                      'טלפון 1': 'first_phone',
                      'טלפון 2': 'second_phone',
                      'טלפון 3': 'third_phone',
                      'כתובת': 'address',
                      'עולה חדש': 'is_new_immigrant',  
                      'סטטוס מלשביות': 'enlistment_status',
                      'קבא': 'kabha',
                      'דפר': 'dapar',
                      'צדכ': 'tzadach',
                      'פרופיל': 'medical_profile',
                      'סעיף רפואי פוסל': 'disqualifying_medical_trait'}


class FieldDayViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows gibush to be viewed or edited.
    """
    queryset = FieldDay.objects.all()
    serializer_class = FieldDaysSerializer
    logging_conf.logger.debug("return all FieldDays")


class FieldDayStatsViewSet(viewsets.ModelViewSet):
    queryset = FieldDaysStats.objects.all()
    serializer_class = FieldDaysStatsProfileSerializer

    def list(self, request, *args, **kwargs):
        participant_id = request.GET.get("id")
        field_day = request.GET.get("field_day")
        if participant_id:
            queryset = FieldDaysStats.objects.get(participant_id=participant_id, field_day=field_day)
            file_serializer = FieldDaysStatsProfileSerializer(queryset)
        else:
            field_day = request.GET.get("FieldDay")
            queryset = FieldDaysStats.objects.filter(field_day=field_day).defer('field_day',
            'fit_hovlim',
            'fit_shayetet',
            'fit_tzolelot',
            'field_day_num',
            'group_number',
            'praiority_matkal',
            'praiority_hovlim',
            'praiority_shayetet',
            'retirement',
            'retirement_reason')
            file_serializer = FieldDaysStatsListSerializer(queryset, many=True)
        return Response(file_serializer.data)
        

class FileUploadView(APIView):
    """
    Title : API endpoint that allows post of pk(gibush) and file name
    Description : using file upload parser to get file.
                  call to fileparse class and call the post function for parsing and analyzing data.
    """
    parser_class = (FileUploadParser,)
    def post(self, request, *args, **kwargs):
        file_serializer = CsvFileSerializer(data=request.data) 
        if file_serializer.is_valid():
            file_serializer.save()
            x = FileParse()
            x.post()
            logging_conf.logger.debug("file uploaded successfully")
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            logging_conf.logger.debug("file doesnt uploaded, error - {}".format(file_serializer.errors))
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FileParse():
    """
    Description: parse csv file, get file name and primarykey(to know the file location)
    it reads the file(create dict of header and value).
    and create dict of the table header the content from the file. 
    call the clean func for checking and creating objects from csv.
    """
    def __init__(self):
        super(FileParse).__init__()

    def parse_csv(self, csv_filename, csv_pk): 
        with open(os.path.join(MEDIA_ROOT, str(csv_filename)), encoding="ISO-8859-8") as csvfile:
            reader = csv.DictReader(csvfile)
            row_number = 1
            for row in reader:
                row_participant_dict = {}
                row_field_days_dict = {}
                row_number += 1
                for file_key in row.keys():
                    table_file_key = file_key.replace(u'\xa0', " ").strip()
                    if table_file_key in TABLE_FILE_HEADERS.keys():
                        row_participant_dict[TABLE_FILE_HEADERS.get(table_file_key)] = row.get(file_key)
                        
                    if table_file_key in TABLE_FIELD_DAYS_FILE_HEADERS.keys():
                        row_field_days_dict[TABLE_FIELD_DAYS_FILE_HEADERS.get(table_file_key)] = row.get(file_key)
                row_participant_dict['source_file'] = os.path.join(MEDIA_ROOT, str(csv_filename))
                for file_key in row.keys():
                    table_file_key = file_key.replace(u'\xa0', " ").strip()

                participant = Participent.objects.clean(row_participant_dict, row_number)
                if participant:
                    row_field_days_dict["participant_id"] = list(participant.values())[0].get("id")
                    error = FieldDaysStats.objects.clean(row_field_days_dict, row_number)
                    if error:
                        return error
        return Response("success")

    def post(self, *args, **kwargs):
        try:
            csv_obj = CsvFile.objects.order_by('-id')[0]
            response = self.parse_csv(csv_obj.csv_file, csv_obj.date_file)
            return Response("success", status=status.HTTP_201_CREATED)
        except CsvParseError as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)
