from django.urls import include, path
from rest_framework import routers
from api.fielddays import views
from gibushim_django import settings
from django.conf.urls.static import static

# default router of  of viewsets - creates endpoints to be viewed or edited.
router = routers.DefaultRouter()
router.register(r'fieldday', views.FieldDayViewSet)
router.register(r'fieldday_stats', views.FieldDayStatsViewSet)





# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    path('upload_file/', views.FileUploadView.as_view()),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
