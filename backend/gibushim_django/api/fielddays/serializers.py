from django.contrib.auth.models import User, Group
from api.models import FieldDay, CsvFile, FieldDaysStats, Participent
from rest_framework import serializers
from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator
from django.core.files.base import ContentFile
import base64
import six
import uuid
import imghdr
from gibushim_django import settings
import os
from django.conf.urls.static import static
from api.gibush.serializers import ParticipentSerializer

class FieldDaysSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = FieldDay
        fields = ['pk','start_date', 'end_date', 'title', 'description']


class CsvFileSerializer(serializers.HyperlinkedModelSerializer):
    """
    serializer of filename and date for save file in model
    """
    class Meta:
        model = CsvFile
        fields = ["date_file", "csv_file"]


class FileParseSerializer(serializers.Serializer):
    """
    serializer of filename and pk for open file
    """
    csv_filename = serializers.CharField()
    csv_pk = serializers.CharField()


class FieldDaysStatsProfileSerializer(serializers.HyperlinkedModelSerializer):
    participant = ParticipentSerializer(read_only=True)
    assessors_grades_average = serializers.SerializerMethodField()
    field_day_properties = FieldDaysSerializer(read_only=True)

    class Meta:
        model = FieldDaysStats
        fields = ['participant',
                'field_day_properties',
                'assessors_grades_average',
                'fit_matkal',
                'fit_hovlim',
                'fit_shayetet',
                'fit_tzolelot',
                'field_day_num',
                'group_number',
                'first_assessor',
                'second_assessor',
                'first_assessor_grade',
                'second_assessor_grade',
                'baror_grade',
                'sozio_grade',
                'praiority_matkal',
                'praiority_hovlim',
                'praiority_shayetet',
                'retirement',
                'retirement_reason']


    def get_assessors_grades_average(self, instance):
        return (instance.first_assessor_grade + instance.second_assessor_grade) / 2

"""
    def get_participant(self, instance):
        #soldier = Participent.objects.get(id=instance.participant)
        if instance.participant:
            return ParticipentSerializer(instance.participant, read_only=True).data
"""
    
"""
    def get_field_day_properties(self, instance):
        if instance.field_day:
            return FieldDaysSerializer(instance.field_day, read_only=True).data
"""


class FieldDaysStatsListSerializer(serializers.HyperlinkedModelSerializer):
    participant = ParticipentSerializer(read_only=True)
    assessors_grades_average = serializers.SerializerMethodField()

    class Meta:
        model = FieldDaysStats
        fields = ['participant',
                'assessors_grades_average',
                'fit_matkal',
                'first_assessor',
                'second_assessor',
                'first_assessor_grade',
                'second_assessor_grade',
                'baror_grade',
                'sozio_grade']


    def get_assessors_grades_average(self, instance):
        return (instance.first_assessor_grade + instance.second_assessor_grade) / 2