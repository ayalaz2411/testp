from django.test import TestCase, Client
import pytest

# Create your tests here.
class test_divide_group(Client):
    def __init__(self):
        super(test_divide_group).__init__()

    def test_create_gibush(self, start_date, end_date, title, description):
        response = self.post('/gibush/', {'start_date': start_date,
                'end_date': end_date,
                'title': title,
                'description': description})
        assert response.status_code == 201, "insert gibush worked"
        assert response.status_code != 201, response

    def test_create_groups(self, num_of_groups, stage):
        response = self.post('/gibush_group/', {'num_of_groups': num_of_groups,
                            'stage': stage}
                            )
        assert response.status_code == 201, "insert worked"
        assert response.status_code != 201, response

    def test_split_to_groups(self, myun_id, num_of_groups):
        response = self.post('/split_groups/', {'myun_id':myun_id, 'num_of_groups': num_of_groups})
        assert response == "success", "splited participents into groups"
        assert response != "success", response

    def test_add_assessors(self, first_name, last_name, identity_number, unit):
        response = self.post('/Assessor/', {'first_name':first_name,
                        'last_name':last_name,
                        'identity_number': identity_number,
                        'unit': unit})
        assert response.status_code == 201, "insert worked"
        assert response.status_code != 201, response

    def test_add_assessors_to_group(self, group_id, assessor_id, role):
        response = self.post('/assessor_group/', {"group_id": group_id,
                        "assessor_id": assessor_id,
                        "role": role})
        assert response.status_code == 201, "insert worked"
        assert response.status_code != 201, response

def test_schema():
    first_names = ["איתי", "אביב", "רועי", "כלב", "ארז"]
    last_names = ["גדי", "בר", "בן יקר", "ים", "קנר"]
    print(13)
    identity_numbers = ["123132312", "434654456", "434654421", "434654413", "434654423"]
    unit = ["872", "872", "123", "098", "454"]
    client = test_divide_group()
    client.test_create_gibush('10-5-2021', '20-5-2021', "גיבוש הגברים", "וואלה אחלה גיבוש")
    client.test_create_groups('10', 'א')
    for i in range(len(first_names)):
        client.test_add_assessors(first_names[i], last_names[i], identity_numbers[i], unit[i])
        client.test_add_assessors_to_group(str(i),str(i),"מפקד")
    client.test_split_to_groups('1', '10')

def main():
    test_schema()


if __name__ == "__main__":
    main()