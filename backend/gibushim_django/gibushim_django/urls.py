from django.urls import include, path
from rest_framework import routers

from gibushim_django import settings
from django.conf.urls.static import static

# default router of  of viewsets - creates endpoints to be viewed or edited.
"""
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'gibush', views.GibushViewSet)
router.register(r'Participent', views.ParticipentViewSet)
router.register(r'Assessor', views.AssessorViewSet)
router.register(r'gibush_stats', views.EnterGibushStatsView)
router.register(r'assessor_eval', views.AssessorEvaluationView)
router.register(r'SozioGibush', views.SozioGibsuhGradesViewSet)
router.register(r'SozioRoute', views.SozioRouteGradesViewSet)
router.register(r'TrustWorthy', views.SozioRouteTrustWorthyViewSet)
router.register(r'retirements', views.RetirementsView)
router.register(r'teams', views.TeamViewSet)
router.register(r'route_soldier', views.RouteSoldierViewSet)
router.register(r'team_soldiers', views.TeamSoldiersViewSet)
"""
router = routers.DefaultRouter()


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    path('route/', include('api.route.urls')),
    path('gibush/', include('api.gibush.urls')),
    path('fieldday/', include('api.fielddays.urls')),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

"""
path('filled_documents/', views.FilledDocuments.as_view()),
    path('import_gibush/', views.ImportGibush.as_view()),
    path('export_gibush/', views.export_participant.as_view()),
    path('download_groups/', views.OutputGroupsView.as_view()),
    path('sozio_pdf/', views.ReturnSozioFiles),
    path('download_grades/', views.OutputGradesView.as_view()),
    path('upload_file/', views.FileUploadView.as_view()),
    path('all_grades/', views.AllGradesView.as_view()),
    path('assessor_group/', views.AssessorInGroupView.as_view()),
    path('gibush_group/', views.GibushGroupsView.as_view()),
    path('participent_list/', views.ReturnParticipentListSpecificGibush.as_view()),
    path('participent_profile/', views.ParticipentProfileView.as_view()),
    
"""