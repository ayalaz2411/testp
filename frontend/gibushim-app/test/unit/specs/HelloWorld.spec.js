import Vue from 'vue'
import MainLayout from '@/components/MainLayout'

describe('MainLayout.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(MainLayout)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.hello h1').textContent)
  })
})
