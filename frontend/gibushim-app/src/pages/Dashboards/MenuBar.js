import HomeMenuBar from "./HomeMenuBar";
import FieldMenuBar from "./FieldMenuBar";

var MenuBar = null;
if (process.env.FIELDSYSTEM) {
  MenuBar = FieldMenuBar;
} else {
  MenuBar = HomeMenuBar;
}

export default MenuBar;
