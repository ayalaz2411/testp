import Vue from "vue";
import Router from "vue-router";
import AddGibush from "@/pages/Gibushim/AddGibush";
import AddParticipantsToGibush from "@/pages/Gibushim/AddParticipantsToGibush";
import DummyPage from "@/pages/Dummy/DummyPage";
import Gibushim from "@/pages/Gibushim/Gibushim";
import ChooseGroup from "@/pages/Gibushim/Groups/ChooseGroup";
import Gibush from "@/pages/Gibushim/Gibush";
import AssessorList from "@/pages/Assessors/AssessorList";
import ParticipantsList from "@/pages/Participants/ParticipantsList";
import Levels from "@/pages/Levels/Levels";
import FieldDays from "@/pages/FieldDays/FieldDays";
import FieldDay from "@/pages/FieldDays/FieldDay";
import AddFieldDay from "@/pages/FieldDays/AddFieldDay";
import ParticipantProfile from "@/pages/Participants/ParticipantProfile";
import RouteSoldierProfile from "@/pages/Participants/ParticipantProfile/RouteSoldierProfile";
import FieldDayProfile from "@/pages/Participants/ParticipantProfile/FieldDayProfile";
import AssessorEval from "@/pages/Assessors/AssessorEval";
import InsertValues from "@/pages/Levels/InsertValues";
import Groups from "@/pages/Gibushim/Groups/Groups";
import Soziometry from "@/pages/Gibushim/Groups/Soziometry";
import ReceiveParticipant from "@/pages/Participants/ReceiveParticipant";
import RetiredParticipantList from "@/pages/Participants/RetiredParticipantList";
import ParticipantListStage from "@/pages/Participants/ParticipantListStage";
import ParticipantListGroup from "@/pages/Participants/ParticipantListGroup";
import GibushResults from "@/pages/Gibushim/GibushResults";
import StageResults from "@/pages/Levels/StageResults";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Main",
      component: Gibushim
    },
    {
      path: "/gibushim",
      name: "Gibushim",
      component: Gibushim
    },
    {
      path: "/gibushim/gibush/:id",
      name: "Gibush",
      component: Gibush,
      props: true
    },
    {
      path: "/gibushim/gibush/:gibush_id/:gibush_title/results",
      name: "GibushResults",
      component: GibushResults
    },
    {
      path: "/gibushim/gibush/:gibush_id/stage/:level_phase/results",
      name: "StageResults",
      component: StageResults
    },
    {
      path: "/gibushim/gibush/:gibush_id/level/:level_phase/choose_group/",
      name: "ChooseGroup",
      component: ChooseGroup
    },
    {
      path: "/gibushim/add-gibush",
      name: "AddGibush",
      component: AddGibush
    },
    {
      path: "/gibushim/add-gibush/add-participants",
      name: "AddParticipantsToGibush",
      component: AddParticipantsToGibush
    },
    {
      path: "/assessors/gibush/:gibush_id",
      name: "AssessorList",
      component: AssessorList
    },
    {
      path: "/participants/gibush/:gibush_id",
      name: "ParticipantsList",
      component: ParticipantsList
    },
    {
      path: "/participants/gibush/:gibush_id/stage/:level_phase",
      name: "ParticipantListStage",
      component: ParticipantListStage
    },
    {
      path:
        "/participants/gibush/:gibush_id/stage/:level_phase/group/:group_id",
      name: "ParticipantListGroup",
      component: ParticipantListGroup
    },
    {
      path: "/dummy",
      name: "DummyPage",
      component: DummyPage
    },
    {
      path: "/gibushim/gibush/:gibush_id/level/:level_phase/",
      name: "Levels",
      component: Levels
    },
    {
      path: "/gibushim/gibush/:gibush_id/group/:group_id/levels",
      name: "GroupLevels",
      component: Levels
    },
    {
      path: "/participants/gibush/:gibush_id/participant/:id",
      name: "ParticipantProfile",
      component: ParticipantProfile
    },
    {
      path:
        "/gibushim/gibush/:gibush_id/level/:level_phase/group/:group_id/assessor-evaluation",
      name: "AssessorEval",
      component: AssessorEval
    },
    {
      path:
        "/gibushim/gibush/:gibush_id/level/:level_phase/group/:group_id/insert-data",
      name: "InsertValues",
      component: InsertValues
    },
    {
      path: "/gibushim/gibush/:gibush_id/level/:level_phase/list_groups/",
      name: "Groups",
      component: Groups
    },
    {
      path:
        "/gibushim/gibush/:gibush_id/level/:level_phase/group/:group_id/soziometry",
      name: "Soziometry",
      component: Soziometry
    },
    {
      path:
        "/gibushim/gibush/:gibush_id/level/:level_phase/receive-participants",
      name: "ReceiveParticipant",
      component: ReceiveParticipant
    },
    {
      path: "/gibushim/gibush/:gibush_id/retired",
      name: "RetiredParticipantList",
      component: RetiredParticipantList
    }
  ]
});
