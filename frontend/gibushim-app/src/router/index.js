import fieldRouter from "./fieldGibush";
import homeRouter from "./homeGibush";
var router = null;

if (process.env.FIELDSYSTEM) {
  router = fieldRouter;
} else {
  router = homeRouter;
}

export default router;
