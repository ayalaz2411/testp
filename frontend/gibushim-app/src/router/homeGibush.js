import Vue from "vue";
import Router from "vue-router";
import AddGibush from "@/pages/Gibushim/AddGibush";
import AddParticipantsToGibush from "@/pages/Gibushim/AddParticipantsToGibush";
import DummyPage from "@/pages/Dummy/DummyPage";
import Main from "@/pages/Dashboards/Main";
import Gibushim from "@/pages/Gibushim/Gibushim";
import ChooseGroup from "@/pages/Gibushim/Groups/ChooseGroup";
import Gibush from "@/pages/Gibushim/Gibush";
import AssessorList from "@/pages/Assessors/AssessorList";
import ParticipantsList from "@/pages/Participants/ParticipantsList";
import Levels from "@/pages/Levels/Levels";
import FieldDays from "@/pages/FieldDays/FieldDays";
import FieldDay from "@/pages/FieldDays/FieldDay";
import AddFieldDay from "@/pages/FieldDays/AddFieldDay";
import ParticipantProfile from "@/pages/Participants/ParticipantProfile";
import FullParticipantProfile from "@/pages/Participants/ParticipantProfile/FullParticipantProfile";
import RouteSoldierProfile from "@/pages/Participants/ParticipantProfile/RouteSoldierProfile";
import FieldDayProfile from "@/pages/Participants/ParticipantProfile/FieldDayProfile";
import AssessorEval from "@/pages/Assessors/AssessorEval";
import InsertValues from "@/pages/Levels/InsertValues";
import Groups from "@/pages/Gibushim/Groups/Groups";
import Soziometry from "@/pages/Gibushim/Groups/Soziometry";
import ReceiveParticipant from "@/pages/Participants/ReceiveParticipant";
import RetiredParticipantList from "@/pages/Participants/RetiredParticipantList";
import ParticipantListStage from "@/pages/Participants/ParticipantListStage";
import ParticipantListGroup from "@/pages/Participants/ParticipantListGroup";
import GibushResults from "@/pages/Gibushim/GibushResults";
import StageResults from "@/pages/Levels/StageResults";
import Teams from "@/pages/Teams/Teams";
import Team from "@/pages/Teams/Team";
import TeamMembers from "@/pages/Teams/TeamMembers";
import AddTeam from "@/pages/Teams/AddTeam";
import CommanderEvaluation from "@/pages/Teams/CommanderEvaluation";
import ChooseLeader from "@/pages/Teams/ChooseLeader";
import AddMembers from "@/pages/Teams/AddMembers";
import TeamAssessment from "../pages/Teams/TeamAssessment/TeamAssessment";
import TeamSozioResults from "../pages/Teams/TeamSozioResults";
import TeamSozioSummary from "../pages/Teams/TeamSozioSummary";
import AddParticipantsToFieldDay from "@/pages/FieldDays/AddParticipantsToFieldDay";
import TeamSozioComment from "../pages/Teams/TeamSozioComment";
Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Main",
      component: Main
    },
    {
      path: "/gibushim",
      name: "Gibushim",
      component: Gibushim
    },
    {
      path: "/gibushim/gibush/:id",
      name: "Gibush",
      component: Gibush,
      props: true
    },
    {
      path: "/gibushim/gibush/:gibush_id/:gibush_title/results",
      name: "GibushResults",
      component: GibushResults
    },
    {
      path: "/gibushim/gibush/:gibush_id/stage/:level_phase/results",
      name: "StageResults",
      component: StageResults
    },
    {
      path: "/gibushim/gibush/:gibush_id/level/:level_phase/choose_group/",
      name: "ChooseGroup",
      component: ChooseGroup
    },
    {
      path: "/gibushim/add-gibush",
      name: "AddGibush",
      component: AddGibush
    },
    {
      path: "/gibushim/add-gibush/add-participants",
      name: "AddParticipantsToGibush",
      component: AddParticipantsToGibush
    },
    {
      path: "/assessors/gibush/:gibush_id",
      name: "AssessorList",
      component: AssessorList
    },
    {
      path: "/participants/gibush/:gibush_id",
      name: "ParticipantsList",
      component: ParticipantsList
    },
    {
      path: "/participants/gibush/:gibush_id/stage/:level_phase",
      name: "ParticipantListStage",
      component: ParticipantListStage
    },
    {
      path:
        "/participants/gibush/:gibush_id/stage/:level_phase/group/:group_id",
      name: "ParticipantListGroup",
      component: ParticipantListGroup
    },
    {
      path: "/dummy",
      name: "DummyPage",
      component: DummyPage
    },
    {
      path: "/gibushim/gibush/:gibush_id/level/:level_phase/",
      name: "Levels",
      component: Levels
    },
    {
      path: "/gibushim/gibush/:gibush_id/group/:group_id/levels",
      name: "GroupLevels",
      component: Levels
    },
    {
      path: "/fielddays",
      name: "FieldDays",
      component: FieldDays
    },
    {
      path: "/fielddays/add-field-day",
      name: "AddFieldDay",
      component: AddFieldDay
    },
    {
      path: "/fielddays/add-field-day/add-participants",
      name: "AddParticipantsToFieldDay",
      component: AddParticipantsToFieldDay
    },
    {
      path: "/fielddays/fieldday/:id",
      name: "FieldDay",
      component: FieldDay
    },
    {
      path: "/participants/gibush/:gibush_id/participant/:id",
      name: "ParticipantProfile",
      component: ParticipantProfile
    },
    {
      path:
        "/gibushim/gibush/:gibush_id/level/:level_phase/group/:group_id/assessor-evaluation",
      name: "AssessorEval",
      component: AssessorEval
    },
    {
      path:
        "/gibushim/gibush/:gibush_id/level/:level_phase/group/:group_id/insert-data",
      name: "InsertValues",
      component: InsertValues
    },
    {
      path: "/gibushim/gibush/:gibush_id/level/:level_phase/list_groups/",
      name: "Groups",
      component: Groups
    },
    {
      path:
        "/gibushim/gibush/:gibush_id/level/:level_phase/group/:group_id/soziometry",
      name: "Soziometry",
      component: Soziometry
    },
    {
      path:
        "/gibushim/gibush/:gibush_id/level/:level_phase/receive-participants",
      name: "ReceiveParticipant",
      component: ReceiveParticipant
    },
    {
      path: "/gibushim/gibush/:gibush_id/retired",
      name: "RetiredParticipantList",
      component: RetiredParticipantList
    },
    {
      path: "/Teams/team/:team_id/:team_name/retired",
      name: "RetiredRouteParticipantList",
      component: RetiredParticipantList
    },
    {
      path: "/Teams/",
      name: "Teams",
      component: Teams
    },
    {
      path: "/Teams/team/:team_id/:team_name/",
      name: "Team",
      component: Team
    },
    {
      path: "/Teams/team/:team_id/:team_name/members/",
      name: "TeamMembers",
      component: TeamMembers
    },
    {
      path: "/Teams/team/:team_id/:team_name/assessment/",
      name: "TeamAssessment",
      component: TeamAssessment
    },
    {
      path: "/Teams/AddTeam/",
      name: "AddTeam",
      component: AddTeam
    },
    {
      path: "/Teams/AddTeam/:team_name/ChooseLeader/",
      name: "ChooseLeader",
      component: ChooseLeader
    },
    {
      path: "/Teams/AddMembers/team/:team_name/id/:team_id/",
      name: "AddMembers",
      component: AddMembers
    },
    {
      path: "/Teams/AddMembers/team/:team_name/id/:team_id/results",
      name: "TeamSozioResults",
      component: TeamSozioResults
    },
    {
      path:
        "/Teams/AddMembers/team/:team_name/id/:team_id/results/:SozioNumber/summary",
      name: "TeamSozioSummary",
      component: TeamSozioSummary
    },
    {
      path:
        "/Teams/AddMembers/team/:team_name/id/:team_id/Commander_Evaluation",
      name: "CommanderEvaluation",
      component: CommanderEvaluation
    },
    {
      path:
        "/Teams/AddMembers/team/:team_name/id/:team_id/results/sozio-stage/:sozio_stage/comments/soldier/:soldier_id/",
      name: "TeamSozioComment",
      component: TeamSozioComment
    },
    {
      path: "/participants/:id/",
      name: "FullParticipantProfile",
      component: FullParticipantProfile,
      children: [
        {
          path: "profile/myun_id/:gibush_id",
          name: "ParticipantProfile1",
          component: ParticipantProfile,
          props: true
        },
        {
          path: "profile/team_id/:team_id",
          name: "RouteSoldierProfile",
          component: RouteSoldierProfile,
          props: true
        },
        {
          path: "profile/field_day/:field_day",
          name: "FieldDayProfile",
          component: FieldDayProfile,
          props: true
        }
      ]
    }
  ]
});
