// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { BootstrapVue, IconsPlugin, AvatarPlugin, VBHoverPlugin } from 'bootstrap-vue'
import VCalendar from 'v-calendar'
import WebCam from 'vue-web-cam'
import _ from 'lodash'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'zingchart/es6'
import zingchartVue from 'zingchart-vue'

Vue.component('zingchart', zingchartVue)

// All of out imports
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(AvatarPlugin)
Vue.use(VBHoverPlugin)
Vue.use(VCalendar)
Vue.use(WebCam)
Vue.directive('visible', (el, bind) => {
  el.style.visibility = bind.value ? 'visible' : 'hidden'
})
Object.defineProperty(Vue.prototype, '$_', { value: _ })

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
