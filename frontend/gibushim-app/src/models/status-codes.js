const HttpStatusCodes = {
  OK: 200,
  CREATED: 201
}

export default HttpStatusCodes
