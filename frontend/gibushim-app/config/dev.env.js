"use strict";
const merge = require("webpack-merge");
const prodEnv = require("./prod.env");

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  FIELDSYSTEM: "true",
  BASIC_API: '"http://localhost:8000"',
  ROOT_API: '"http://localhost:8000/gibush"'
});
